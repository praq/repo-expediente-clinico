-- REGISTRO EN LA BITACORA DE ACCIONES EN PACIENTE
CREATE OR REPLACE FUNCTION NuePaciente() RETURNS TRIGGER AS $nuevoPaci$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista ha registrado un nuevo Paciente.', current_timestamp );
RETURN NULL;
END;
$nuevoPaci$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_paciente AFTER INSERT 
ON "Paciente" FOR EACH ROW
EXECUTE PROCEDURE NuePaciente();

CREATE OR REPLACE FUNCTION UpPaciente() RETURNS TRIGGER AS $UpPaci$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista ha Actualizado el paciente.', current_timestamp );
RETURN NULL;
END;
$UpPaci$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_paciente AFTER UPDATE 
ON "Paciente" FOR EACH ROW
EXECUTE PROCEDURE UpPaciente();

-- REGISTRO EN LA BITACORA DE ACCIONES EN TRABAJADOR
CREATE OR REPLACE FUNCTION NueTrabajador() RETURNS TRIGGER AS $nuevoTra$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha registrado un nuevo trabajador.', current_timestamp );
RETURN NULL;
END;
$nuevoTra$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_trabajador AFTER INSERT 
ON "Empleado" FOR EACH ROW
EXECUTE PROCEDURE NueTrabajador();

CREATE OR REPLACE FUNCTION UpTrabajador() RETURNS TRIGGER AS $upTra$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha actualizado la informacion del trabajador.', current_timestamp );
RETURN NULL;
END;
$upTra$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_trabajador AFTER UPDATE
ON "Empleado" FOR EACH ROW
EXECUTE PROCEDURE UpTrabajador();

-- REGISTRO EN LA BITACORA DE ACCIONES EN COSTOS
CREATE OR REPLACE FUNCTION NueCosto() RETURNS TRIGGER AS $nuevoCos$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha registrado un nuevo Costo.', current_timestamp );
RETURN NULL;
END;
$nuevoCos$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_costo AFTER INSERT 
ON "Costos_servicios" FOR EACH ROW
EXECUTE PROCEDURE NueCosto();

CREATE OR REPLACE FUNCTION upCosto() RETURNS TRIGGER AS $upCosto$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha actualizado un  Costo.', current_timestamp );
RETURN NULL;
END;
$upCosto$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_costo AFTER UPDATE
ON "Costos_servicios" FOR EACH ROW
EXECUTE PROCEDURE upCosto();

-- REGISTRO EN LA BITACORA DE ACCIONES EN CLINICA
CREATE OR REPLACE FUNCTION NueClinica() RETURNS TRIGGER AS $nuevoCli$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha registrado una nueva clinica.', current_timestamp );
RETURN NULL;
END;
$nuevoCli$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_clinica AFTER INSERT 
ON "Clinica" FOR EACH ROW
EXECUTE PROCEDURE  NueClinica();

CREATE OR REPLACE FUNCTION UpClinica() RETURNS TRIGGER AS $UpClinica$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador ha actualizado una  clinica.', current_timestamp );
RETURN NULL;
END;
$UpClinica$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_clinica AFTER UPDATE 
ON "Clinica" FOR EACH ROW
EXECUTE PROCEDURE  UpClinica();

-- REGISTRO EN LA BITACORA DE ACCIONES EN DIAGNOSTICO MEDICO
CREATE OR REPLACE FUNCTION NueDiagcli() RETURNS TRIGGER AS $nuevoDiag$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor ha registrado un nuevo diagnostico clinico.', current_timestamp );
RETURN NULL;
END;
$nuevoDiag$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_diagnosticoClinico AFTER INSERT 
ON "Diagnostico_medico" FOR EACH ROW
EXECUTE PROCEDURE  NueDiagcli();

CREATE OR REPLACE FUNCTION UpDiagcli() RETURNS TRIGGER AS $UpDiagcli$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor ha actualizado la informacion de  diagnostico clinico.', current_timestamp );
RETURN NULL;
END;
$UpDiagcli$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_diagnosticoClinico AFTER UPDATE 
ON "Diagnostico_medico" FOR EACH ROW
EXECUTE PROCEDURE  UpDiagcli();


-- REGISTRO EN LA BITACORA DE ACCIONES EN ESPECIALIDAD
CREATE OR REPLACE FUNCTION NueEsp() RETURNS TRIGGER AS $nuevaEsp$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El Administrador ha registrado una nueva especialidad', current_timestamp );
RETURN NULL;
END;
$nuevaEsp$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_especialidad AFTER INSERT 
ON "Especialidad" FOR EACH ROW
EXECUTE PROCEDURE  NueEsp();

CREATE OR REPLACE FUNCTION UpEsp() RETURNS TRIGGER AS $UpEsp$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El Administrador ha actualizado una  especialidad', current_timestamp );
RETURN NULL;
END;
$UpEsp$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_especialidad AFTER UPDATE 
ON "Especialidad" FOR EACH ROW
EXECUTE PROCEDURE  UpEsp();

-- REGISTRO EN LA BITACORA DE ACCIONES EN CONSULTAS
CREATE OR REPLACE FUNCTION NueCons() RETURNS TRIGGER AS $nuevaCons$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista asigno una nueva consulta.', current_timestamp );
RETURN NULL;
END;
$nuevaCons$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_consulta AFTER INSERT 
ON "Consulta" FOR EACH ROW
EXECUTE PROCEDURE  NueCons();

CREATE OR REPLACE FUNCTION UpCons() RETURNS TRIGGER AS $UpCons$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor atendio una  cosulta.', current_timestamp );
RETURN NULL;
END;
$UpCons$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_consulta AFTER UPDATE 
ON "Consulta" FOR EACH ROW
EXECUTE PROCEDURE  UpCons();


-- REGISTRO EN LA BITACORA DE ACCIONES EN CONTACTO DE EMERGENCIA
CREATE OR REPLACE FUNCTION NueCont() RETURNS TRIGGER AS $nuevaCont$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista asigno un nuevo contacto de emergencia a un paciente.', current_timestamp );
RETURN NULL;
END;
$nuevaCont$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_contEmergencia AFTER INSERT 
ON "Contacto_emergencia" FOR EACH ROW
EXECUTE PROCEDURE  NueCont();

CREATE OR REPLACE FUNCTION UpCont() RETURNS TRIGGER AS $UpCont$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista actualizo el  contacto de emergencia a un paciente.', current_timestamp );
RETURN NULL;
END;
$UpCont$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_contEmergencia AFTER UPDATE 
ON "Contacto_emergencia" FOR EACH ROW
EXECUTE PROCEDURE  UpCont();




-- REGISTRO EN LA BITACORA DE ACCIONES EN FAMILIAR
CREATE OR REPLACE FUNCTION NueFam() RETURNS TRIGGER AS $nuevofam$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista registro un nuevo familiar en el sistema.', current_timestamp );
RETURN NULL;
END;
$nuevofam$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_familiar AFTER INSERT 
ON "Familiar" FOR EACH ROW
EXECUTE PROCEDURE  NueFam();


CREATE OR REPLACE FUNCTION UpFam() RETURNS TRIGGER AS $UpFam$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La recepcionista actualizo un  familiar en el expediente de un paciente en el sistema.', current_timestamp );
RETURN NULL;
END;
$UpFam$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_familiar AFTER UPDATE 
ON "Familiar" FOR EACH ROW
EXECUTE PROCEDURE  UpFam();


-- REGISTRO EN LA BITACORA DE ACCIONES EN iNGRESO
CREATE OR REPLACE FUNCTION NueIngre() RETURNS TRIGGER AS $nuevoIngre$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor registro un ingreso en el hospital', current_timestamp );
RETURN NULL;
END;
$nuevoIngre$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ingresod AFTER INSERT 
ON "Ingreso" FOR EACH ROW
EXECUTE PROCEDURE NueIngre();

CREATE OR REPLACE FUNCTION UpIngre() RETURNS TRIGGER AS $UpIngre$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor Actualizo el  ingreso en el hospital', current_timestamp );
RETURN NULL;
END;
$UpIngre$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ingresod AFTER UPDATE 
ON "Ingreso" FOR EACH ROW
EXECUTE PROCEDURE UpIngre();

-- REGISTRO EN LA BITACORA DE ACCIONES EN Medicacion Control
CREATE OR REPLACE FUNCTION NueMedact() RETURNS TRIGGER AS $nuevaMedact$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor registro una nueva medicacion', current_timestamp );
RETURN NULL;
END;
$nuevaMedact$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_medicacion AFTER INSERT 
ON "Medicacion_actual" FOR EACH ROW
EXECUTE PROCEDURE NueMedact();

CREATE OR REPLACE FUNCTION UpMedact() RETURNS TRIGGER AS $UpMedact$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor Actualizo la medicacion asignada', current_timestamp );
RETURN NULL;
END;
$UpMedact$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_medicacion AFTER INSERT 
ON "Medicacion_actual" FOR EACH ROW
EXECUTE PROCEDURE UpMedact();


-- REGISTRO EN LA BITACORA DE ACCIONES EN Medicacion
CREATE OR REPLACE FUNCTION NueMed() RETURNS TRIGGER AS $nuevaMed$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor registro una nueva orden de medicamento', current_timestamp );
RETURN NULL;
END;
$nuevaMed$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_medicamento AFTER INSERT 
ON "Medicamento" FOR EACH ROW
EXECUTE PROCEDURE NueMed();

CREATE OR REPLACE FUNCTION UpMed() RETURNS TRIGGER AS $UpMed$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor Actualizo  orden de medicamento', current_timestamp );
RETURN NULL;
END;
$UpMed$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_medicamento AFTER UPDATE
ON "Medicamento" FOR EACH ROW
EXECUTE PROCEDURE UpMed();

-- REGISTRO EN LA BITACORA DE ACCIONES EN Orden Examen
CREATE OR REPLACE FUNCTION NueOrdExa() RETURNS TRIGGER AS $nuevaMed$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor registro una nueva orden de Examen', current_timestamp );
RETURN NULL;
END;
$nuevaMed$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_orden_examen AFTER INSERT 
ON "Orden_examen" FOR EACH ROW
EXECUTE PROCEDURE NueOrdExa();

CREATE OR REPLACE FUNCTION UpOrdExa() RETURNS TRIGGER AS $UpOrdExa$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor actualizo la orden de orden de Examen', current_timestamp );
RETURN NULL;
END;
$UpOrdExa$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_orden_examen AFTER UPDATE
ON "Orden_examen" FOR EACH ROW
EXECUTE PROCEDURE UpOrdExa();

-- REGISTRO EN LA BITACORA DE ACCIONES EN Orden Examen
CREATE OR REPLACE FUNCTION NueRece() RETURNS TRIGGER AS $nuevaRece$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor registro una nueva Receta', current_timestamp );
RETURN NULL;
END;
$nuevaRece$ LANGUAGE plpgsql;

CREATE TRIGGER nueva_Receta AFTER INSERT 
ON "Receta" FOR EACH ROW
EXECUTE PROCEDURE NueRece();

CREATE OR REPLACE FUNCTION UpRece() RETURNS TRIGGER AS $UpRece$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El doctor actualizo la informacion de la  Receta', current_timestamp );
RETURN NULL;
END;
$UpRece$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_Receta AFTER UPDATE
ON "Receta" FOR EACH ROW
EXECUTE PROCEDURE UpRece();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN FISICO
CREATE OR REPLACE FUNCTION ExFisico() RETURNS TRIGGER AS $ExFisico$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen fisico', current_timestamp );
RETURN NULL;
END;
$ExFisico$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExFisico AFTER INSERT 
ON "Resultado_e_fisicoisico" FOR EACH ROW
EXECUTE PROCEDURE ExFisico();

CREATE OR REPLACE FUNCTION upExFisico() RETURNS TRIGGER AS $upExFisico$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen fisico', current_timestamp );
RETURN NULL;
END;
$upExFisico$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExFisico AFTER UPDATE
ON "Resultado_e_fisicoisico" FOR EACH ROW
EXECUTE PROCEDURE upExFisico();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN Hematologico
CREATE OR REPLACE FUNCTION ExHema() RETURNS TRIGGER AS $ExHema$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen Hematologico', current_timestamp );
RETURN NULL;
END;
$ExHema$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExHema AFTER INSERT 
ON "Resultado_e_hematologicotologico" FOR EACH ROW
EXECUTE PROCEDURE ExHema();

CREATE OR REPLACE FUNCTION upExHema() RETURNS TRIGGER AS $upExHema$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen Hematologico', current_timestamp );
RETURN NULL;
END;
$upExHema$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExHema AFTER UPDATE 
ON "Resultado_e_hematologicotologico" FOR EACH ROW
EXECUTE PROCEDURE upExHema();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN HECES MACRO
CREATE OR REPLACE FUNCTION ExHeMacro() RETURNS TRIGGER AS $ExHeMacro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen heces macroscopico.', current_timestamp );
RETURN NULL;
END;
$ExHeMacro$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExHeMacro AFTER INSERT 
ON "Resultado_examen_heces_macroscopico" FOR EACH ROW
EXECUTE PROCEDURE ExHeMacro();

CREATE OR REPLACE FUNCTION upExHeMacro() RETURNS TRIGGER AS $upExHeMacro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen heces macroscopico.', current_timestamp );
RETURN NULL;
END;
$upExHeMacro$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExHeMacro AFTER UPDATE 
ON "Resultado_examen_heces_macroscopico" FOR EACH ROW
EXECUTE PROCEDURE upExHeMacro();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN HECES MICRO
CREATE OR REPLACE FUNCTION ExHeMicro() RETURNS TRIGGER AS $ExHeMicro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen heces microscopico.', current_timestamp );
RETURN NULL;
END;
$ExHeMicro$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExHeMicro AFTER INSERT 
ON "Resultado_examen_heces_microscopico" FOR EACH ROW
EXECUTE PROCEDURE ExHeMicro();

CREATE OR REPLACE FUNCTION upExHeMicro() RETURNS TRIGGER AS $upExHeMicro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen heces microscopico.', current_timestamp );
RETURN NULL;
END;
$upExHeMicro$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExHeMicro AFTER UPDATE 
ON "Resultado_examen_heces_microscopico" FOR EACH ROW
EXECUTE PROCEDURE upExHeMicro();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN HECES QUIMICO
CREATE OR REPLACE FUNCTION ExHeQui() RETURNS TRIGGER AS $ExHeQui$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen heces Quimico.', current_timestamp );
RETURN NULL;
END;
$ExHeQui$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExHeQui AFTER INSERT 
ON "Resultado_examen_heces_quimicos" FOR EACH ROW
EXECUTE PROCEDURE ExHeQui();

CREATE OR REPLACE FUNCTION upExHeQui() RETURNS TRIGGER AS $upExHeQui$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen heces Quimico.', current_timestamp );
RETURN NULL;
END;
$upExHeQui$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExHeQui AFTER UPDATE 
ON "Resultado_examen_heces_quimicos" FOR EACH ROW
EXECUTE PROCEDURE upExHeQui();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN ORINA CRISTALUIRA
CREATE OR REPLACE FUNCTION ExOriCri() RETURNS TRIGGER AS $ExOriCri$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen Orina Cristaluria.', current_timestamp );
RETURN NULL;
END;
$ExOriCri$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExOriCri AFTER INSERT 
ON "Resultado_examen_orina_cristaluria" FOR EACH ROW
EXECUTE PROCEDURE ExOriCri();

CREATE OR REPLACE FUNCTION upExOriCri() RETURNS TRIGGER AS $upExOriCri$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen Orina Cristaluria.', current_timestamp );
RETURN NULL;
END;
$upExOriCri$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExOriCri AFTER UPDATE
ON "Resultado_examen_orina_cristaluria" FOR EACH ROW
EXECUTE PROCEDURE upExOriCri();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN ORINA MACROSCOPICO
CREATE OR REPLACE FUNCTION ExOriMacro() RETURNS TRIGGER AS $ExOriMacro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen Orina Macroscopico.', current_timestamp );
RETURN NULL;
END;
$ExOriMacro$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExOriMacro AFTER INSERT 
ON "Resultado_examen_orina_macroscopico" FOR EACH ROW
EXECUTE PROCEDURE ExOriMacro();

CREATE OR REPLACE FUNCTION upExOriMacro() RETURNS TRIGGER AS $upExOriMacro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen Orina Macroscopico.', current_timestamp );
RETURN NULL;
END;
$upExOriMacro$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExOriMacro AFTER UPDATE 
ON "Resultado_examen_orina_macroscopico" FOR EACH ROW
EXECUTE PROCEDURE upExOriMacro();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN ORINA MICROSCOPICO
CREATE OR REPLACE FUNCTION ExOriMicro() RETURNS TRIGGER AS $ExOriMicro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen Orina Microscopico.', current_timestamp );
RETURN NULL;
END;
$ExOriMicro$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExOriMicro AFTER INSERT 
ON "Resultado_examen_orina_microscopico" FOR EACH ROW
EXECUTE PROCEDURE ExOriMicro();

CREATE OR REPLACE FUNCTION upExOriMicro() RETURNS TRIGGER AS $upExOriMicro$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen Orina Microscopico.', current_timestamp );
RETURN NULL;
END;
$upExOriMicro$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExOriMicro AFTER UPDATE
ON "Resultado_examen_orina_microscopico" FOR EACH ROW
EXECUTE PROCEDURE upExOriMicro();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN ORINA Quimico
CREATE OR REPLACE FUNCTION ExOriQui() RETURNS TRIGGER AS $ExOriQui$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen Orina Quimico.', current_timestamp );
RETURN NULL;
END;
$ExOriQui$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExOriQui AFTER INSERT 
ON "Resultado_examen_orina_quimico" FOR EACH ROW
EXECUTE PROCEDURE ExOriQui();

CREATE OR REPLACE FUNCTION upExOriQui() RETURNS TRIGGER AS $upExOriQui$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizo los resultados de un examen Orina Quimico.', current_timestamp );
RETURN NULL;
END;
$upExOriQui$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExOriQui AFTER UPDATE 
ON "Resultado_examen_orina_quimico" FOR EACH ROW
EXECUTE PROCEDURE upExOriQui();

-- REGISTRO EN LA BITACORA DE ACCIONES EN EXAMEN  QUIMICO SANGUINEO
CREATE OR REPLACE FUNCTION ExQuiSa() RETURNS TRIGGER AS $ExQuiSA$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista registro los resultados de un examen  Quimico Sanquineo.', current_timestamp );
RETURN NULL;
END;
$ExQuiSA$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_ExQuiSA AFTER INSERT 
ON "Resultado_examen_quimico_sanguineo" FOR EACH ROW
EXECUTE PROCEDURE ExQuiSA();

CREATE OR REPLACE FUNCTION upExQuiSa() RETURNS TRIGGER AS $upExQuiSA$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El laboratorista actualizar los resultados de un examen  Quimico Sanquineo.', current_timestamp );
RETURN NULL;
END;
$upExQuiSA$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_ExQuiSA AFTER UPDATE
ON "Resultado_examen_quimico_sanguineo" FOR EACH ROW
EXECUTE PROCEDURE upExQuiSA();

-- REGISTRO EN LA BITACORA DE ACCIONES EN Signo_vital
CREATE OR REPLACE FUNCTION SigVit() RETURNS TRIGGER AS $SigVit$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La Enfermera registro un nuevo signo vital del paciente', current_timestamp );
RETURN NULL;
END;
$SigVit$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_SigVit AFTER INSERT 
ON "Signo_vital" FOR EACH ROW
EXECUTE PROCEDURE SigVit();

CREATE OR REPLACE FUNCTION upSigVit() RETURNS TRIGGER AS $upSigVit$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La Enfermera actualizo signo vital del paciente', current_timestamp );
RETURN NULL;
END;
$upSigVit$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_SigVit AFTER UPDATE 
ON "Signo_vital" FOR EACH ROW
EXECUTE PROCEDURE upSigVit();
-- REGISTRO EN LA BITACORA DE ACCIONES EN Turno
CREATE OR REPLACE FUNCTION turno() RETURNS TRIGGER AS $turno$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La rececpcionista registro un nuevo turno y se asignaron doctores a este turno', current_timestamp );
RETURN NULL;
END;
$turno$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_turno AFTER INSERT 
ON "Turno" FOR EACH ROW
EXECUTE PROCEDURE turno();

CREATE OR REPLACE FUNCTION upturno() RETURNS TRIGGER AS $upturno$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('La rececpcionista actualizo turno y se asignaron doctores a este turno', current_timestamp );
RETURN NULL;
END;
$upturno$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_turno AFTER UPDATE
ON "Turno" FOR EACH ROW
EXECUTE PROCEDURE upturno();

-- REGISTRO EN LA BITACORA DE ACCIONES EN USUARUIOS
CREATE OR REPLACE FUNCTION usuario() RETURNS TRIGGER AS $usuario$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador Creo un nuevo usuario', current_timestamp );
RETURN NULL;
END;
$usuario$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_usuario AFTER INSERT 
ON "auth_user" FOR EACH ROW
EXECUTE PROCEDURE usuario();

CREATE OR REPLACE FUNCTION upusuario() RETURNS TRIGGER AS $upusuario$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador actualizo la informacion de un usuario', current_timestamp );
RETURN NULL;
END;
$upusuario$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_usuario AFTER UPDATE 
ON "auth_user" FOR EACH ROW
EXECUTE PROCEDURE upusuario();

-- REGISTRO EN LA BITACORA DE ACCIONES EN GRUPOS
CREATE OR REPLACE FUNCTION grupo() RETURNS TRIGGER AS $grupo$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador asigno un nuevo al usuario un grupod de permisos', current_timestamp );
RETURN NULL;
END;
$grupo$ LANGUAGE plpgsql;

CREATE TRIGGER nuevo_grupo AFTER INSERT 
ON "auth_user_groups" FOR EACH ROW
EXECUTE PROCEDURE grupo();

CREATE OR REPLACE FUNCTION upgrupo() RETURNS TRIGGER AS $upgrupo$
DECLARE BEGIN 
INSERT INTO "Control" (accion, "Fecha_realizacion") VALUES ('El administrador actualizo el grupo al que petenece un usuario', current_timestamp );
RETURN NULL;
END;
$upgrupo$ LANGUAGE plpgsql;

CREATE TRIGGER actualizar_grupo AFTER UPDATE
ON "auth_user_groups" FOR EACH ROW
EXECUTE PROCEDURE upgrupo();

----FIN TRIGGERS Y FUNCIONES DE LA BITACORA DEL SISTEMA

------ VIEWS DEL SISTEMA--------------------
CREATE VIEW stats_examenes AS
SELECT  row_number() OVER() AS id, id_tipo as id_tipo_id, COUNT(id_tipo ) AS cantidad FROM "Orden_examen" group by id_tipo;

CREATE VIEW totuser AS
SELECT  row_number() OVER() AS id, COUNT(id) AS usuariossistema FROM "auth_user";

CREATE VIEW totempleados AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntempleado FROM "Empleado";

CREATE VIEW totpacientes AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntpaciente FROM "Paciente";

CREATE VIEW consatendidas AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntconsultas FROM "Consulta";

CREATE VIEW usudepart AS
SELECT  row_number() OVER() AS id, departamento_id_id as id_departamento_id_id , COUNT(departamento_id_id ) AS cantidad FROM "Persona" group by departamento_id_id;

CREATE VIEW cntespecialidades AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntespecialidades FROM "Especialidad";

CREATE VIEW cntclinicas AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cnteclinicas FROM "Clinica";

CREATE VIEW conteosalas AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntsalas FROM "Sala";

CREATE VIEW contcostos AS
SELECT  row_number() OVER() AS id, COUNT(id) AS cntcostos FROM "Costos_servicios";

CREATE VIEW citasatendidadporfecha AS
SELECT  row_number() OVER() AS id, id_turno as id_turno_id , fecha_reservacion as fechaatencion, COUNT(fecha_reservacion) AS cantidad FROM "Consulta" group by fecha_reservacion, id_turno;