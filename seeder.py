# Para usar la app para para poblar la base con este archivo:
import django
django.setup()
from apps.account.models import *
from datetime import date
from django.core.management import call_command
from apps.personas.models import *
from apps.consultas.models import *
from apps.antecedentes.models import *
from django_seed import Seed
import random
import datetime


# Instalar la app segun el archivo requirements.txt
# Descomentar el nombre (django_seed) de la app en settings en INSATALED_APPS
# Ejecutar en sh:
# export DJANGO_SETTINGS_MODULE=ExpedienteClinicoProject.settings
# Crear superusurio: echo "from django.contrib.auth.models import User; User.objects.create_superuser('sparkadmin', 'admin@sparkclinic.com', 'spark123')" | ./manage.py shell


#Funcion
def cargar_usuarios(num=5):
    from django_seed import Seed
    seeder = Seed.seeder()
    seeder.add_entity(User, num, {
        'password': lambda x: seeder.faker.password(),
        'is_superuser': lambda x: False,
        'username': lambda x: seeder.faker.user_name()+seeder.faker.numerify("####")
    })
    pks_users = seeder.execute()[User]
    return pks_users
#Funcion
def crear_mujer_soltera(nacimiento,uid):
    from django_seed import Seed
    seeder = Seed.seeder()
    muni=Municipio.objects.order_by("?").first()
    persona = Persona.objects.create(
            primer_nombre=seeder.faker.first_name_female(),
            primer_apellido=seeder.faker.last_name(),
            segundo_nombre=seeder.faker.first_name_female(),
            segundo_apellido=seeder.faker.last_name(),
            apellido_casada="",
            genero="femenino",
            dui=seeder.faker.numerify("########-#"),
            telefono="7"+seeder.faker.numerify("###-####"),
            colonia=seeder.faker.city(),
            calle=seeder.faker.street_address( ),
            casa=seeder.faker.building_number(),
            nit=seeder.faker.numerify("####-######-###-#"),
            isss=int(seeder.faker.numerify("#########")),
            fecha_nacimiento=nacimiento,
            estado_civil=random.choice(['soltera','divorciada',"acompañada"]),
            auth_id_id=uid,
            departamento_id=muni.departamento_id,
            municipio_id=muni
        )
    return persona
#Funcion
def crear_mujer_casada(nacimiento,uid):
    from django_seed import Seed
    seeder = Seed.seeder()
    muni=Municipio.objects.order_by("?").first()
    persona = Persona.objects.create(
            primer_nombre=seeder.faker.first_name_female(),
            primer_apellido=seeder.faker.last_name(),
            segundo_nombre=seeder.faker.first_name_female(),
            segundo_apellido=seeder.faker.last_name(),
            apellido_casada=seeder.faker.last_name_female(),
            genero="femenino",
            dui=seeder.faker.numerify("########-#"),
            telefono="7" + seeder.faker.numerify("###-####"),
            colonia=seeder.faker.city(),
            calle=seeder.faker.street_address(),
            casa=seeder.faker.building_number(),
            nit=seeder.faker.numerify("####-######-###-#"),
            isss=int(seeder.faker.numerify("#########")),
            fecha_nacimiento=nacimiento,
            estado_civil=random.choice(['casada', 'viuda']),
            auth_id_id=uid,
            departamento_id=muni.departamento_id,
            municipio_id=muni
        )
    return persona
#Funcion
def crear_hombre(nacimiento,uid):
    from django_seed import Seed
    seeder = Seed.seeder()
    muni = Municipio.objects.order_by("?").first()
    persona = Persona.objects.create(
        primer_nombre=seeder.faker.first_name_male(),
        primer_apellido=seeder.faker.last_name(),
        segundo_nombre=seeder.faker.first_name_male(),
        segundo_apellido=seeder.faker.last_name(),
        apellido_casada="",
        genero="masculino",
        dui=seeder.faker.numerify("########-#"),
        telefono="7" + seeder.faker.numerify("###-####"),
        colonia=seeder.faker.city(),
        calle=seeder.faker.street_address(),
        casa=seeder.faker.building_number(),
        nit=seeder.faker.numerify("####-######-###-#"),
        isss=int(seeder.faker.numerify("#########")),
        fecha_nacimiento=nacimiento,
        estado_civil=random.choice(
            ['casado', 'viudo', 'divorciado', 'soltero', 'acompañado']),
        auth_id_id=uid,
        departamento_id=muni.departamento_id,
        municipio_id=muni
    )

    return persona

#Directo
def cargar_json(nombres=['DepartMunicipios.json', 'CodigoInternacional.json', 'TipoExamenes.json']):
    for nombre in nombres:
        call_command('loaddata', nombre)
#Directo
def cargar_clinica(num=5):
    from django_seed import Seed
    print("Clinica")
    muni = Municipio.objects.order_by("?").first()
    seeder = Seed.seeder()
    seeder.add_entity(Clinica, num, {
        'nombre': lambda x: seeder.faker.company(),
        'municipio_id': lambda x: muni,
        'departamento_id': lambda x: muni.departamento_id,
        'direccion': lambda x: seeder.faker.address(),
    })
    pks_clinicas = seeder.execute()[Clinica]
    return pks_clinicas
#Directo
def cargar_roles():
    print("Permisos")
    paciente = Group.objects.create(name='Paciente')
    enfermera = Group.objects.create(name='Enfermera')
    doctor = Group.objects.create(name='Doctor')
    labo = Group.objects.create(name='Laboratorista')
    admin = Group.objects.create(name='Administrador')
    recep = Group.objects.create(name='Recepcionista')

    permisos = Permission.objects.filter(
        codename__in=['expedientes_index', 'examenes_ver', 'consultas_ver', 'cita_control'])
    paciente.permissions.add(*permisos)

    permisos = Permission.objects.filter(
        codename__in=['expedientes_index', 'examenes_ver', 'consultas_ver', 'signos_editar'])
    enfermera.permissions.add(*permisos)

    permisos = Permission.objects.filter(codename__in=[
                                         'expedientes_index', 'examenes_ver', 'consultas_ver', 'consultas_editar'])
    doctor.permissions.add(*permisos)

    permisos = Permission.objects.filter(codename__in=['laboratorio_control'])
    labo.permissions.add(*permisos)

    permisos = Permission.objects.filter(codename__in=[
                                         'clinica_control', 'usuarios_control', 'permisos_control', 'empleados_control', 'costos_control', 'especialidad_control', 'reportes_control'])
    admin.permissions.add(*permisos)

    permisos = Permission.objects.filter(codename__in=[
                                         'turnos_control', 'cita_control', 'expediente_control', 'expedientes_index'])
    recep.permissions.add(*permisos)
#Directo
def cargar_costos():
    print("Costos")
    lista = ["Aplicación de inyección de penicilina", "Consulta de Especialidad",
        "Aplicación de inyección de insulina", "Consulta General", "Ingreso de 3 días",
        "Terapia  Respiratoria", "Cirugía Ambulatoria", "Curación de herida menor",
        "Curación de herida grave", "Examen Fisico", "Examen Hematologico", "Examen Heces Macroscopico",
        "Examen Heces Microscopico", "Examen Heces Quimico", "Examen Orina Cristaluria",
        "Examen Orina Macroscopico", "Examen Orina Microscopico", "Examen Orina Quimico",
        "Examen Quimico Sanguineo"]

    for i in range(0, len(lista)):
        CostosServicios.objects.create(
            concepto=lista[i],
            costo=random.triangular(low=30.00, high=75.00, mode=50.00))
#Directo
def cargar_personal_admin():
    from django_seed import Seed
    seeder = Seed.seeder()
    print("Admins")
    for cli in range(1, Clinica.objects.count() + 1):
        pks_users = cargar_usuarios(1)

        uid = pks_users[0]
        nombre_g = "Recepcionista"

        grupo = Group.objects.get(name=nombre_g)
        usuario = User.objects.get(pk=uid)
        usuario.groups.add(grupo)

        nacimiento = seeder.faker.date_of_birth(
            tzinfo=None, minimum_age=23, maximum_age=99)

        if cli % 3 == 0:
            persona = crear_hombre(nacimiento, uid)
        elif cli % 2 == 0:
            persona = crear_mujer_soltera(nacimiento, uid)
        else:
            persona = crear_mujer_casada(nacimiento, uid)

        empleado = Empleado.objects.create(
            tipo_empleado=grupo,
            sueldo=random.choice([301.50, 310.90, 450.33, 375.75]),
            cargo="Recepcionista",
            fecha_contratacion=seeder.faker.date_of_birth(
                tzinfo=None, minimum_age=1, maximum_age=4),
            estado=1,
            id_clinica=Clinica.objects.get(pk=cli),
            id_especialidad=None,
            id_persona=persona
        )

        cargos = ["Director Generaral", "Subdirector General",
                  "Director de Gestión"]

        for c in range(0, len(cargos)):
            pks_users = cargar_usuarios(1)

            uid = pks_users[0]
            nombre_g = "Administrador"

            grupo = Group.objects.get(name=nombre_g)
            usuario = User.objects.get(pk=uid)
            usuario.groups.add(grupo)

            nacimiento = seeder.faker.date_of_birth(
                tzinfo=None, minimum_age=23, maximum_age=99)

            if (c * cli) % 3 == 0:
                persona = crear_hombre(nacimiento, uid)
            elif (c * cli) % 2 == 0:
                persona = crear_mujer_soltera(nacimiento, uid)
            else:
                persona = crear_mujer_casada(nacimiento, uid)

            empleado = Empleado.objects.create(
                tipo_empleado=grupo,
                sueldo=random.choice([600.50, 500.50, 700.00,
                                     955.50, 1200.00, 1050.90]),
                cargo=cargos[c],
                fecha_contratacion=seeder.faker.date_of_birth(
                    tzinfo=None, minimum_age=1, maximum_age=4),
                estado=1,
                id_clinica=Clinica.objects.get(pk=cli),
                id_especialidad=None,
                id_persona=persona
            )
#Directo
def cargar_turnos():#Sin parametros
    from django_seed import Seed
    seeder = Seed.seeder()
    print("Turnos")
    #Crear un turno matutino y uno vespertino en Julio
    fecha = datetime.date(2020, 7, 31)
    turno = Turno.objects.create(
        nombre_turno = "Turno "+seeder.faker.color()+" "+seeder.faker.numerify("##"),
        fecha_inicio = fecha.replace(day=1),
        fecha_fin = fecha,
        hora_inicio = datetime.time(6,0,0),
        hora_fin = datetime.time(12,0,0),
        activo = True,
        capacidad = 25,
    )

    for i in range(0, random.randint(2,12)):
        turno.doctores.add(
            Empleado.objects.filter(tipo_empleado="Personal Medico")
            .order_by("?").first())

    turno = Turno.objects.create(
        nombre_turno = "Turno "+seeder.faker.color()+" "+seeder.faker.numerify("##"),
        fecha_inicio = fecha.replace(day=1),
        fecha_fin = fecha,
        hora_inicio = datetime.time(12,00,00),
        hora_fin = datetime.time(18,00,00),
        activo = True,
        capacidad = 25,
    )

    for i in range(0, random.randint(2,12)):
        turno.doctores.add(
            Empleado.objects.filter(tipo_empleado="Personal Medico")
            .order_by("?").first())

    #Crear un turno por cada mes de enero a junio
    fecha = datetime.date(2020,6,28)
    for i in range(0,5):
        turno = Turno.objects.create(
            nombre_turno = "Turno "+seeder.faker.color()+" "+seeder.faker.numerify("###"),
            fecha_inicio = fecha.replace(day=1),
            fecha_fin = fecha.replace(month=fecha.month-i),
            hora_inicio = datetime.time(6,0,0),
            hora_fin = datetime.time(17,0,0),
            activo = False,
            capacidad = 30,
        )
        for i in range(0, random.randint(2,7)):
            turno.doctores.add(
                Empleado.objects.filter(tipo_empleado="Personal Medico")
                .order_by("?").first())
#Directo
def crear_salas(sal=5, cam=10,cli=0):
    from django_seed import Seed
    seeder = None
    seeder = Seed.seeder()
    seeder.add_entity(Sala, 5, {
        'id_clinica': lambda x: Clinica.objects.get(pk=cli),
        'nombre_sala': lambda x: random.choice(["Dr. ","Dra. ","San "]) + seeder.faker.last_name(),
        'tipo_sala': lambda x: random.choice(["Cirugía Ambulatoria","Cirugía Programada","Cuidados Intensivos"])
    })

    pks_salas = seeder.execute()[Sala]

    print(pks_salas)
    print(sal)

    for j in pks_salas:
        print(cam)
        for i in range(1, 10):
            sala = Camilla.objects.create(
                id_sala=Sala.objects.get(pk=j),
                numero_camilla=(((j+1)*i) if j == 0 else i*j),
            )
    pks_salas = None
def cargar_salas_final(cam=10,sal=5):
    id_p = Clinica.objects.first().id
    for cli in range(1, Clinica.objects.count()+1):
        crear_salas(sal,cam,id_p)
        id_p = id_p + cli

#Directo
def crear_paciente(num=5):
    print("Paciente")
    seeder = Seed.seeder()
    pks_users = cargar_usuarios(num)
    for i in range(0, num):

        uid = pks_users[i]

        grupo = Group.objects.get(name='Paciente')
        usuario = User.objects.get(pk=uid)
        usuario.groups.add(grupo)

        nacimiento = seeder.faker.date_of_birth(
            tzinfo=None, minimum_age=18, maximum_age=99)

        if i % 3 == 0:
            persona = crear_mujer_soltera(nacimiento, uid)
        elif i % 2 == 0:
            persona = crear_mujer_casada(nacimiento, uid)
        else:
            persona = crear_hombre(nacimiento, uid)

        paciente = Paciente.objects.create(
            id_persona=persona,
            NumExp=seeder.faker.bothify(text='????###-##')) #En que formato está el codigo de expediente

        if i % 2 == 0:
            contacto = ContactoEmergencia.objects.create(
                nombre_contacto=seeder.faker.name_female(),
                telefono="2" + seeder.faker.numerify("###-####"),
                parentesco=random.choice(
                    ["Cuñada", "Hermana", "Abuela",
                     "Novia", "Mamá", "Tía", "Hija", "Sobrina"]),
                id_paciente=paciente
            )
        else:
            contacto = ContactoEmergencia.objects.create(
                nombre_contacto=seeder.faker.name_male(),
                telefono="2" + seeder.faker.numerify("###-####"),
                parentesco=random.choice(
                    ["Cuñado", "Compañero de vida",
                     "Esposo", "Papá", "Tío", "Hijo", "Sobrino"]),
                id_paciente=paciente
            )

        ante_personal = AntecedentesPersonal.objects.create(
            alergias=seeder.faker.text(max_nb_chars=200),
            id_paciente=paciente
        )

        for e in range(0, random.randint(1, 7)):
            ante_medico = AntecedentesMedicos.objects.create(
                fecha=nacimiento.replace(
                    year=nacimiento.year + random.randint(0, 10)),
                id_antecedentes_personal=ante_personal,
                id_codigo=CodigoInternacional.objects.order_by("?").first()
            )

        for e in range(0, random.randint(1, 4)):
            ante_familiar = AntecedentesFamiliares.objects.create(
                nombre_familiar=seeder.faker.name_female(),
                parentesco=random.choice(
                    ["Bisabuela", "Abuela", "Mamá", "Tía", "Hermana"]),
                padecimientos=seeder.faker.sentence(nb_words=7),
                id_paciente=paciente
            )
            ante_familiar = AntecedentesFamiliares.objects.create(
                nombre_familiar=seeder.faker.name_male(),
                parentesco=random.choice(
                    ["Bisabuelo", "Abuelo", "Papá", "Tío", "Hermano"]),
                padecimientos=seeder.faker.sentence(nb_words=7),
                id_paciente=paciente
            )

        for e in range(0, random.randint(0, 3)):
            ante_quirur = AntecedentesQuirurgico.objects.create(
                descripcion_cirugia=seeder.faker.sentence(nb_words=5),
                complicacion=seeder.faker.text(max_nb_chars=150),
                fecha=nacimiento.replace(year=nacimiento.year + 7),
                causa=seeder.faker.sentence(nb_words=5),
                id_antecedentes_personal=ante_personal
            )
        for e in range(0, random.randint(0, 3)):
            medicacion_actual = MedicacionActual.objects.create(
                nombre_medicamento=random.choice(
                    ["Bencidamina", "Ácido Acetilsalicílico", "Metocarbamol", "Ibuprofeno", "Ibuprofeno", "Naproxeno", "Carbonato de Calcio", "Compuestos de Magnesio", "Compuestos de Aluminio", "Magaldrato", "Magaldrato", "Loperamida", "Bromuro de N-Butilhioscina", "Mebeverina Clorhidrato", "Clorhidrato de Propinoxato", "Simeticona", "Simeticona", "Bromuro de Clidinio", "Bicarbonato de sodio", "Dimenhidrinato", "Glicerina", "Lanzoprazol", "Ranitidina", "Psyllium Plántago", "Preparado paregórico alcanforado", "Kaolin Coloidal", "Sucralfato", "Probióticos"]),
                nombre_comercial_me=random.choice(["Aspirina Omeprazol" , "Lexotiroxina sódica", "Ramipril" ,    "Amlodipina" , "Paracetamol" , "Atorvastatina" ,    "Salbutamol" ,  "Lansoprazol" , "Hidrocloruro de metformina ",  "Colecalciferol" , "    Bisoprolol fumarato" ,  "Co-codamol" ,  "Bendroflumetiazida" ,  "Citalopram hidrobromuro" ,  "amoxicilina" , "furosemida" , "amitriptilina hidrocloruro" , "Warfarina sódica ", "anticoagulante"]),
                concentracion=random.choice(["325 mg", "120 mg/5mL", "100mg/mL","250 mg", "500 mg", "100mg/5mL", "40 mg", "800 mg", "800 mg/5mL", "10mg", "125mg", "2.5 mg", "75 mg / 5 mL", "0.2%", "1 g"]),
                frecuencia=str(seeder.faker.random_int(min=1, max=4))+" cada dia",
                tipo_administracion=random.choice(["oral", "sublingual", "rectal", "intradérmica", "subcutánea", "intramuscular"]),
                id_antecedente_personal=ante_personal
            )

        for s in range(0, random.randint(2,7)):
            signo = SignoVital.objects.create(
                temperatura=random.triangular(low=28.00,high=43.00,mode=35.00),
                frecuencia_cardiaca=random.randint(50,100),
                frecuencia_respiratoria=random.randint(12,30),
                presion_arterial=str(random.randint(50,80))+"/"+str(random.randint(80,200)),
                peso=random.triangular(low=100.00,high=500.00,mode=200.00),
                paciente=paciente
            )

            fecha=signo.fecha

            mes = fecha.month - random.randint(2,7)
            if mes < 1 :
                mes = 1

            if mes == 2 and fecha.day > 28:
                fecha=fecha.replace(day=28)

            fecha=fecha.replace(month=mes)

            dia = fecha.day - random.randint(2,20)
            if dia < 1 :
                dia = 1

            signo.fecha=fecha.replace(day=dia)

        for s in range(0, random.randint(2,7)):
            emp = Empleado.objects.order_by('?').first()
            turno = Turno.objects.order_by('?').first()

            fecha = turno.fecha_inicio
            fecha = fecha.replace(day=fecha.day + random.randint(2, 20))

            consulta = Consulta.objects.create(
                    id_paciente=paciente,
                    id_empleado=emp,
                    fecha_reservacion=fecha,
                    turno=turno,
                )

            dia = fecha.day + random.randint(2, 10)
            if dia > 28:
                dia = 28

            fecha_i = fecha.replace(day=dia)

            dia = fecha_i.day + random.randint(2, 10)
            if dia > 28 or fecha_i.day == 28:
                dia = 28
                if fecha_i.day == 28:
                    mes = fecha_i.month + 1
                    if mes > 12:
                        mes = 1

            fecha_e = fecha_i.replace(day=dia)

            if s % 2 == 0:
                ingreso = Ingreso.objects.create(
                    id_consulta=consulta,
                    id_camilla=Camilla.objects.order_by('?').first(),
                    fecha_ingreso=fecha_i,
                    fecha_egreso=fecha_e,
                )

            diagnostico = DiagnosticoMedico.objects.create(
                id_consulta=consulta,
                sintomas=seeder.faker.sentence(nb_words=9)
            )

            for x in range(0, random.randint(1, 4)):
                diagnostico.id_codigo_internacional.add(CodigoInternacional.objects.order_by('?').first())

            for r in range(0,random.randint(1,3)):
                receta = Receta.objects.create(
                    id_diagnostico=diagnostico
                )
                for m in range(1,random.randint(1,5)):
                    medicamento = Medicamento.objects.create(
                        nombre=random.choice(["Bencidamina", "Ácido" "Acetilsalicílico", "Metocarbamol", "Ibuprofeno", "Ibuprofeno", "Naproxeno", "Carbonato de Calcio", "Compuestos de Magnesio", "Compuestos de Aluminio", "Magaldrato", "Magaldrato", "Loperamida", "Bromuro de N-Butilhioscina", "Mebeverina Clorhidrato", "Clorhidrato de Propinoxato", "Simeticona", "Simeticona", "Bromuro de Clidinio", "Bicarbonato de sodio", "Dimenhidrinato", "Glicerina", "Lanzoprazol", "Ranitidina", "Psyllium Plántago", "Preparado paregórico alcanforado", "Kaolin Coloidal", "Sucralfato", "Probióticos"]),
                        via_administracion=random.choice(["oral", "sublingual", "rectal", "intradérmica", "subcutánea", "intramuscular"]),
                        nombre_comercial=random.choice(["Aspirina    Omeprazol" ,    "Lexotiroxina sódica" , "Ramipril" ,    "Amlodipina" , "Paracetamol" , "Atorvastatina" ,    "Salbutamol" ,  "Lansoprazol" , "Hidrocloruro de metformina ",  "Colecalciferol" , "    Bisoprolol fumarato" ,  "Co-codamol" ,  "Bendroflumetiazida" ,  "Citalopram hidrobromuro" ,  "amoxicilina" , "furosemida" , "amitriptilina hidrocloruro" , "Warfarina sódica ", "anticoagulante"]),
                        concentracion=random.choice(["325 mg", "120 mg/5mL", "100mg/mL", "250 mg", "500 mg", "100mg/5mL", "40 mg", "800 mg", "800 mg/5mL", "10mg", "125mg", "2.5 mg", "75 mg / 5 mL", "0.2%", "1 g"]),
                        frecuencia=str(seeder.faker.random_int(min=1, max=4))+" cada dia",
                        duracion_tratamiento=random.randint(1,30),
                        receta=receta
                    )
        print("Paciente: "+str(i))
#Directo
def crear_personal_salud(num=20):
    print("Personal Salud")
    seeder = Seed.seeder()
    grupos = ["Enfermera", "Laboratorista", "Doctor"]
    pks_users = cargar_usuarios(num)
    for i in range(0, num):

        uid = pks_users[i]
        nombre_g = random.choice(grupos)

        grupo = Group.objects.get(name=nombre_g)
        usuario = User.objects.get(pk=uid)
        usuario.groups.add(grupo)

        nacimiento = seeder.faker.date_of_birth(
            tzinfo=None, minimum_age=23, maximum_age=99)

        if i % 3 == 0:
            persona = crear_hombre(nacimiento, uid)
        elif i % 2 == 0:
            persona = crear_mujer_soltera(nacimiento, uid)
        else:
            persona = crear_mujer_casada(nacimiento, uid)

        espe = None
        if nombre_g == "Laboratorista":
            cargo = random.choice(["Laboratorista", "Jefe de servicio de análisis químico","Director de unidad de Laboratorio"])
        elif nombre_g == "Enfermera":
            cargo = random.choice(["Director de Enfermería","Subdirector de Enfermería","Supervisor de Enfermería","Enfermero"])
        elif nombre_g == "Doctor":
            cargo = random.choice(["Doctor de Planta", "Jefe de Unidad", "Encargado de Sala", "Especialista de Medicina Interna"]),
            espe = Especialidad.objects.order_by("?").first()

        empleado = Empleado.objects.create(
            tipo_empleado="Personal Medico",
            sueldo=random.choice([600.50, 500.50, 700.00,
                                 955.50, 1200.00, 1050.90]),
            cargo=cargo,
            fecha_contratacion=seeder.faker.date_of_birth(
                tzinfo=None, minimum_age=1, maximum_age=4),
            estado=1,
            id_clinica=Clinica.objects.order_by("?").first(),
            id_especialidad=espe,
            id_persona=persona
        )

        print("Personal: "+str(i))


def main():
    # Funciones sin parámetros
    # origenes = ['DepartMunicipios.json',
    #              'CodigoInternacional.json', 'TipoExamenes.json', 'Especialidades.json']
    # cargar_json(origenes)
    # cargar_clinica()
    # cargar_roles()
    # cargar_costos()
    # cargar_personal_admin()

    # Cantidades definidas
    cargar_salas_final(3,4)
    # crear_personal_salud(105)
    # cargar_turnos() #se necesitan doctores creados
    # crear_paciente(500)


main()
