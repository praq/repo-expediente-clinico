from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, REDIRECT_FIELD_NAME
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LogoutView as BaseLogoutView,
    PasswordChangeView as BasePasswordChangeView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView)
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.utils.crypto import get_random_string
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.utils.translation import gettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import View, FormView, ListView, UpdateView, DeleteView, CreateView
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib.auth.decorators import permission_required
from axes.utils import reset
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from . forms import (SignInViaUsernameForm,
                     SignUpForm,
                     SignInViaEmailForm,
                     SignInViaEmailOrUsernameForm,
                     UserForm,
                     UserCreateFormNew,
                     UserChangePassword,
                     AssignGroup,
                     AssignPermissionForm,
                     )
from .models import Activation, User, Group, Permission
from django.contrib.messages.views import SuccessMessageMixin


# Create your views here.

class VisitaView(View):
    def dispatch(self, request, *args, **kwargs):  # ciclo de solicitud se llama a dispatch
        # Redireccionar a la página de index si el usuario ya se autenticó
        if request.user.is_authenticated:
            # variable esta definida en settings
            return redirect(settings.LOGIN_REDIRECT_URL)

        return super().dispatch(request, *args, **kwargs)


class LogInView(VisitaView, FormView):
    template_name = 'account/log_in.html'

    @staticmethod
    def get_form_class(**kwargs):
        if settings.DISABLE_USERNAME or settings.LOGIN_VIA_EMAIL:
            return SignInViaUsernameForm

        if settings.LOGIN_VIA_EMAIL_OR_USERNAME:
            return SignInViaUsernameForm

        return SignInViaUsernameForm

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        # Establece una cookie de prueba para asegurarse de que el usuario tenga habilitadas las cookies.
        request.session.set_test_cookie()

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        request = self.request

        # Si la cookie de prueba funcionó, continúe y elimínela ya que ya no es necesaria
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()

        # La duración predeterminada de "recordarme" de Django es de 2 semanas y se puede cambiar modificando
        # La SESSION_COOKIE_AGE settings'
        if settings.USE_REMEMBER_ME:
            if not form.cleaned_data['remember_me']:
                request.session.set_expiry(0)

        login(request, form.user_cache)

        redirect_to = request.POST.get(
            REDIRECT_FIELD_NAME, request.GET.get(REDIRECT_FIELD_NAME))
        url_is_safe = is_safe_url(
            redirect_to, allowed_hosts=request.get_host(), require_https=request.is_secure())

        if url_is_safe:
            return redirect(redirect_to)

        return redirect(settings.LOGIN_REDIRECT_URL)


class SignUpView(VisitaView, FormView):
    template_name = 'account/sign_up.html'
    form_class = SignUpForm

    def form_valid(self, form):
        request = self.request
        user = form.save(commit=False)

        if settings.DISABLE_USERNAME:
            user.username = get_random_string()
        else:
            user.username = form.cleaned_data['username']

        if settings.ENABLE_USER_ACTIVATION:
            user.is_active = True

        # Crear un registro de usuario
        user.save()

        # Cambie el nombre de usuario al formulario "user_ID"
        if settings.DISABLE_USERNAME:
            user.username = f'user_{user.id}'
            user.save()

        if settings.ENABLE_USER_ACTIVATION:
            code = get_random_string(20)

            act = Activation()
            act.code = code
            act.user = user
            act.save()

        else:
            raw_password = form.cleaned_data['password1']

            user = authenticate(username=user.username, password=raw_password)
            login(request, user)

            messages.success(request, _('Te has registrado correctamente!'))

        return redirect('login')


class LogOutView(LoginRequiredMixin, BaseLogoutView):
    template_name = 'account/log_out.html'


class UserList(ListView):
    model = User
    template_name = 'account/list_user.html'

    def get_queryset(self):
        return self.model.objects.all()


class UserDelete(SuccessMessageMixin, DeleteView):
    model = User
    template_name = 'account/delete_user.html'
    success_message = "Se Elimino el usuario con exito"
    success_url = reverse_lazy('list_user')


class UserUpdate(SuccessMessageMixin, UpdateView):
    model = User
    form_class = UserForm
    template_name = 'account/edit_user.html'
    success_message = "Se Actualizo el usuario con exito"
    success_url = reverse_lazy('list_user')


class UserCreate(SuccessMessageMixin, CreateView):
    model = User
    template_name = 'account/create_user.html'
    form_class = UserCreateFormNew
    success_message = "Se creo con exito el usuario."
    success_url = reverse_lazy('list_user')


class ChangePasswordView(BasePasswordChangeView):
    template_name = 'account/change_password.html'
    form_class = UserChangePassword

    def form_valid(self, form):
        # Change the password
        user = form.save()

        messages.success(self.request, _('Se Cambio tu contraseña.'))

        return redirect('home')


class CreateGroupView(CreateView):
    template_name = 'account/create_group.html'
    form_class = AssignGroup
    success_url = reverse_lazy('list_group')


class ListGroupView(ListView):
    template_name = 'account/list_group.html'
    model = Group

    def get_queryset(self):
        return self.model.objects.all()


class DeleteGroupView(SuccessMessageMixin, DeleteView):
    model = Group
    template_name = 'account/delete_group.html'
    success_message = "Se Elimino el grupo con exito"
    success_url = reverse_lazy('list_group')


class UpdateGroupView(SuccessMessageMixin, UpdateView):
    model = Group
    form_class = AssignGroup
    template_name = 'account/edit_group.html'
    success_message = "Se Actualizo el grupo con exito"
    success_url = reverse_lazy('list_group')


class CreatePermissionView(CreateView):
    template_name = 'account/create_permission.html'
    form_class = AssignPermissionForm
    success_url = reverse_lazy('list_permission')


class ListPermissionView(ListView):
    template_name = 'account/list_permission.html'
    model = Permission

    def get_queryset(self):
        return self.model.objects.all()


class DeletePermissionView(SuccessMessageMixin, DeleteView):
    model = Permission
    template_name = 'account/delete_permission.html'
    success_message = "Se Elimino el Permiso con exito"
    success_url = reverse_lazy('list_permission')


class UpdatePermissionView(SuccessMessageMixin, UpdateView):
    model = Permission
    form_class = AssignPermissionForm
    template_name = 'account/edit_permission.html'
    success_message = "Se Actualizo el permiso con exito"
    success_url = reverse_lazy('list_permission')


class ResetPasswordView(PasswordResetView):
    template_name = 'account/reset_password.html'
    email_template_name = 'account/reset_password_email.html'
    # form_class = UserForm #usar el form que consideren necesario ya sea uno que este hecho o crear uno nuevo
    success_url = reverse_lazy('log_in')


class ResetPasswordDoneView(PasswordResetDoneView):
    template_name = 'account/reset_password_done.html'


class ResetPasswordConfirmView(PasswordResetConfirmView):
    template_name = 'account/reset_password_confirm.html'
    # form_class = UserForm #usar el form que consideren necesario ya sea uno que este hecho o crear uno nuevo

    def form_valid(self, form):

        form.save()
        messages.success(self.request, _(
            'Su contraseña ha sido establecida. Puede continuar e iniciar sesión ahora.'))

        return redirect('log_in')


class ResetPasswordCompleteView(PasswordResetCompleteView):
    template_name = 'account/reset_password_complete.html'


def send_email(mail):
    default_domain = settings.DEFAULT_DOMAIN
    context = {'mail': mail,
               'default_domain': default_domain}
    print(mail)
    template = get_template('account/send_mail_block.html')
    content = template.render(context)
    email = EmailMultiAlternatives(
        'Desbloqueo de usuario', 'Siga el siguiente enlace para desbloquear usuario', settings.EMAIL_HOST_USER, [mail])
    email.attach_alternative(content, 'text/html')
    email.send()

    return redirect(settings.LOGIN_REDIRECT_URL)


def emailsendblock(request):
    if request.method == 'POST':
        mail = request.POST.get('mail')
        send_email(mail)
        print(mail)
        messages.success(request, '1')
    return render(request, 'account/index_mail_block.html', {})


def axesresetuser(username):
    user = User.objects.get(username=username)
    if user:
        reset(username=username)
    else:
        response = "No existe el usuario"
        return HttpResponse(response)

    return redirect('log_in')


def resetUser(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        axesresetuser(username)
        print(f"EL USER {username}")
        messages.success(request, '1')
    return render(request, 'account/reset_username.html', {})
