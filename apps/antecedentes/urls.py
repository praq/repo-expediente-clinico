from django.urls import path, include
from apps.antecedentes.views import (antecedente_familiar_create,
									antecedente_familiar_edit,
									antecedente_familiar_delete,
									antecedente_personal_alergias_edit,
									antecedente_personal_medico_create,
									antecedente_personal_medico_edit,
									antecedente_personal_medico_delete,
									antecedente_personal_quirurgico_create,
									antecedente_personal_quirurgico_edit,
									antecedente_personal_quirurgico_delete,
									antecedente_personal_medicacion_actual_create,
									antecedente_personal_medicacion_actual_edit,
									antecedente_personal_medicacion_actual_delete,
									busqueda_codigo,
									busqueda_enfermedad,
									)

urlpatterns = [

	### URLS QUE MANEJAN LOS ANTECEDENTES ###

	#-- Antecedentes Familiares

	#Crear
	path('familiar/crear/<int:id_paciente>/', antecedente_familiar_create, name='ante_familiar_crear'),#Url para Crear nuevo Antecedente Familiar
	#Editar
	path('familiar/editar/',antecedente_familiar_edit, name='ante_familiar_editar'),
	#Eliminar
	path('familiar/<int:id_antecedente_familiar>/eliminar/',antecedente_familiar_delete, name='ante_familiar_eliminar'),

    ##-- Antecedentes Personal - Alergias (Todo paciente creado ya debe tener una intancia del objeto AntecedentesPersonal)

    #NO POSEE CREAR
	#Editar
	path('personal/alergias/editar/<int:id_paciente>/',antecedente_personal_alergias_edit, name='ante_alergias_editar'),
	#NO POSEE ELIMINAR

    #-- Antecedentes Médicos

    #Crear
    path('personal/medico/crear/<int:id_paciente>/',antecedente_personal_medico_create, name='ante_medico_crear'),
	#Editar
	path('personal/medico/editar/',antecedente_personal_medico_edit, name='ante_medico_editar'),
	#Eliminar
	path('personal/medico/<int:id_antecedente_medico>/eliminar/',antecedente_personal_medico_delete, name='ante_medico_eliminar'),

    #-- Antecedentes Quirúrgico

    #Crear
    path('personal/quirurgico/crear/<int:id_paciente>/',antecedente_personal_quirurgico_create, name='ante_quirurgico_crear'),
	#Editar
	path('personal/quirurgico/editar/',antecedente_personal_quirurgico_edit, name='ante_quirurgico_editar'),
	#Eliminar
	path('personal/quirurgico/<int:id_antecedente_quirurgico>/eliminar/',antecedente_personal_quirurgico_delete, name='ante_quirurgico_eliminar'),

    #-- Medicación Actual

    #Crear
    path('personal/medicacion_actual/crear/<int:id_paciente>/',antecedente_personal_medicacion_actual_create, name='ante_medicacion_crear'),
	#Editar
	path('personal/medicacion_actual/editar/',antecedente_personal_medicacion_actual_edit, name='ante_medicacion_editar'),
	#Eliminar
	path('personal/medicacion_actual/<int:id_medicacion_actual>/eliminar/',antecedente_personal_medicacion_actual_delete, name='ante_medicacion_eliminar'),

	#Para ajax Codigo Internacional
	path('ajax/codigo',busqueda_codigo, name="busqueda_codigo"),
	path('ajax/enfermedad',busqueda_enfermedad, name="busqueda_enfermedad"),
]