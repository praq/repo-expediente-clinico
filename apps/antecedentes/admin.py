from django.contrib import admin
from .models import *
# Register your models here.

class AntecedentesPersonalesAdmin(admin.ModelAdmin):
    list_display = ('id','alergias', 'id_paciente')
admin.site.register(AntecedentesPersonal, AntecedentesPersonalesAdmin)

#class AntecedentesMedicosAdmin(admin.ModelAdmin):
#    list_display = ('id','fecha_antecedente_medico', 'id_antecedentes_personal')
#admin.site.register(AntecedentesMedicos, AntecedentesMedicosAdmin)
admin.site.register(AntecedentesMedicos)
#class AntecedentesFamiliaresAdmin(admin.ModelAdmin):
#    list_display = ('id','alergias','padecimientos', 'id_paciente')
#admin.site.register(AntecedetesFamiliares,AntecedentesFamiliaresAdmin)
admin.site.register(AntecedentesFamiliares)

class AntecedentesQuirurgicoAdmin(admin.ModelAdmin):
    list_display = ('id','descripcion_cirugia', 'fecha','causa','id_antecedentes_personal')
admin.site.register(AntecedentesQuirurgico, AntecedentesQuirurgicoAdmin)


class MedicacionActualAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre_medicamento','tipo_administracion')
admin.site.register(MedicacionActual, MedicacionActualAdmin)





