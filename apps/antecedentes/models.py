from django.db import models
from apps.personas.models import Paciente
from apps.consultas.models import CodigoInternacional

# Create your models here.
class AntecedentesPersonal(models.Model):
    #id default
    alergias = models.CharField(max_length=500, default='El paciente declaró que no posee alergias.')
    id_paciente = models.OneToOneField(Paciente, on_delete=models.CASCADE,db_column='id_paciente') #Cambio ForeingKey por OneToOne
    #id_medicacion_actual = models.ForeignKey(MedicacionActual, on_delete=models.CASCADE, db_column='id_medicacion_actual') #No se puede cambiar a medicación actual aunque es lo correcto

    class Meta:
        db_table = 'Antecedentes_personal'
        verbose_name = 'Antecedente personal'
        verbose_name_plural = 'Antecedentes personales'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class AntecedentesMedicos(models.Model):
    #id default
    fecha = models.DateField() #igual definir un estandar con el nombre abreviar o no
    id_antecedentes_personal = models.ForeignKey(AntecedentesPersonal, on_delete=models.CASCADE,db_column='id_antecedentes_personal') # me confundi es el id de antecedentes clinico?
    id_codigo = models.ForeignKey(CodigoInternacional,on_delete=models.CASCADE, db_column='id_codigo') #estandar con llave foranea codigo?


    class Meta:
        db_table = 'Antecedentes_medicos'
        verbose_name = 'Antecedente medico'
        verbose_name_plural = 'Antecedentes medicos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class AntecedentesFamiliares(models.Model):
    #id default
    nombre_familiar = models.CharField(max_length=200)
    parentesco = models.CharField(max_length=50)
    padecimientos = models.CharField(max_length=500, blank=True, null=True)
    id_paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE, db_column='id_paciente') #Cambio OneToOne por ForeignKey

    class Meta:
        db_table = 'Antecedentes_familiares'
        verbose_name = 'Antecedente familiar'
        verbose_name_plural = 'Antecedentes familiares'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class AntecedentesQuirurgico(models.Model):
    #id default
    descripcion_cirugia = models.CharField(max_length=500, default='some_string')
    complicacion= models.CharField(max_length=500,blank=True, null=True)
    fecha = models.DateField()
    causa = models.CharField(max_length=500)
    id_antecedentes_personal= models.ForeignKey(AntecedentesPersonal, on_delete=models.CASCADE,db_column='id_antecedentes_personal')#Cambio relacion de OneToOne a ForeingKey

    class Meta:
        db_table = 'Antecedentes_quirurgico'
        verbose_name = 'Antecedente quirurgico'
        verbose_name_plural = 'Antecedentes quirurgicos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class MedicacionActual(models.Model): #En el diagrama esta minuscula otras clases entidades mayusculas
    #default id
    nombre_medicamento = models.CharField(max_length=100, unique=False)
    nombre_comercial_me = models.CharField(max_length=200, blank=True, null=True)
    concentracion = models.CharField(max_length=200, blank=True, null=True)
    frecuencia = models.CharField(max_length=200)
    tipo_administracion = models.CharField(max_length=200) #en otro modelo se llama via administracion el de Medicamento
    id_antecedente_personal = models.ForeignKey(AntecedentesPersonal, on_delete=models.CASCADE, db_column='id_antecedentes_personal')


    class Meta:
        db_table = 'Medicacion_actual'
        verbose_name = 'Medicacion actual'
        verbose_name_plural = 'Medicaciones actuales'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


