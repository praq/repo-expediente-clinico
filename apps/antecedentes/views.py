from django.shortcuts import render
from django.db.models import F
import json

#funciones
from django.contrib import messages
from django.utils.dateparse import parse_date
from django.contrib.auth.decorators import  permission_required
from django.contrib.auth.models import User
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse

#modelos
from apps.personas.models import *
from apps.consultas.models import *
from apps.antecedentes.models import *

#------------------Antecedentes Familiares------------------------#

#Crear
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_familiar_create(request, id_paciente):
	if request.method=='POST':
		nombre_familiar = request.POST.get('nombre_familiar')
		parentesco=request.POST.get('parentesco')
		padecimientos = request.POST.get('padecimientos')
		paciente = Paciente.objects.get(id = id_paciente)

		new_antecedente_familiar = AntecedentesFamiliares()
		new_antecedente_familiar.nombre_familiar=nombre_familiar
		new_antecedente_familiar.padecimientos = padecimientos
		new_antecedente_familiar.parentesco=parentesco
		new_antecedente_familiar.id_paciente_id = paciente.id


		new_antecedente_familiar.save()
		messages.success(request,'1')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Editar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_familiar_edit(request):
	print("LLEGUE A ESTA MADRE")

	if request.method == 'POST':
		nombre_familiar = request.POST.get('nombre_familiar_editar')
		parentesco=request.POST.get('parentesco_editar')
		padecimientos = request.POST.get('padecimientos_editar')
		identificador = request.POST.get('identificador_editar')
		print(f"*******************************NOMBRE FAMILIAR****** {nombre_familiar}")
		print(f"******IDENTIFICADOR****** {identificador}")
		print(request.POST)

		antecedente_familiar = AntecedentesFamiliares.objects.get(id = identificador)
		antecedente_familiar.nombre_familiar=nombre_familiar
		antecedente_familiar.padecimientos = padecimientos
		antecedente_familiar.parentesco=parentesco

		antecedente_familiar.save()
		messages.success(request,'2')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Eliminar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_familiar_delete(request, id_antecedente_familiar):
	if request.method == 'GET':
		antecedente_delete = AntecedentesFamiliares.objects.get(id=id_antecedente_familiar)
		antecedente_delete.delete()
		messages.success(request,'3')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#------------------Antecedentes Personales - Alergias ------------------------#
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_alergias_edit(request, id_paciente):
	if request.method=='POST':
		alergias=request.POST.get('alergias')
		paciente=Paciente.objects.get(id=id_paciente)

		antecedente=paciente.antecedentespersonal
		antecedente.alergias=alergias
		antecedente.save()

		messages.success(request,'1')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#------------------Antecedentes Médicos  ------------------------#

#Crear
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medico_create(request, id_paciente):
	if request.method=='POST':
		paciente = Paciente.objects.get(id=id_paciente)
		antecedente_personal = paciente.antecedentespersonal
		fecha = request.POST.get('fecha_ante_med_formato')
		id_codigo = request.POST.get('id_cod_ante_med')
		codigo = CodigoInternacional.objects.get(pk=id_codigo)

		new_antecedente_med = AntecedentesMedicos()
		new_antecedente_med.fecha = fecha
		new_antecedente_med.id_codigo = codigo
		new_antecedente_med.id_antecedentes_personal = antecedente_personal
		new_antecedente_med.save()

		messages.success(request,'1')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Editar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medico_edit(request):
	if request.method == 'POST':
		fecha_edit = request.POST.get('fecha_ante_med_editar')
		print(f"La fecha de editar {fecha_edit}")
		id_codigo = request.POST.get('id_cod_ante_med_editar')
		codigo = CodigoInternacional.objects.get(id=id_codigo)
		identificador = request.POST.get('identificador_editar_antmedico')

		antecedente_medico = AntecedentesMedicos.objects.get(id=identificador)
		antecedente_medico.fecha = fecha_edit
		antecedente_medico.id_codigo = codigo
		antecedente_medico.save()

		messages.success(request,'2')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Eliminar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medico_delete(request, id_antecedente_medico):
	if request.method == 'GET':
		antecedente_medico = AntecedentesMedicos.objects.get(id=id_antecedente_medico)
		antecedente_medico.delete()
		messages.success(request,'3')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#------------------Antecedentes Quirúrgicos ------------------------#

#Crear
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_quirurgico_create(request,id_paciente):
	if request.method == 'POST':
		paciente = Paciente.objects.get(id=id_paciente)
		descripcion = request.POST.get('descripcion_cirugia')
		fecha = request.POST.get('fecha_ante_quirur_formato')
		causa = request.POST.get('causa')
		complicacion = request.POST.get('complicacion')
		antecedente_personal = paciente.antecedentespersonal

		new_antecedente_quirur = AntecedentesQuirurgico()
		new_antecedente_quirur.descripcion_cirugia = descripcion
		new_antecedente_quirur.complicacion = complicacion
		new_antecedente_quirur.fecha = fecha
		new_antecedente_quirur.causa = causa
		new_antecedente_quirur.id_antecedentes_personal = antecedente_personal

		new_antecedente_quirur.save()
		messages.success(request, '1')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Editar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_quirurgico_edit(request):
	if request.method == 'POST':
		descripcion = request.POST.get('descripcion_cirugia_editar')
		fecha = request.POST.get('fecha_ante_quirur_editar')
		causa = request.POST.get('causa_editar')
		complicacion = request.POST.get('complicacion_editar')
		identificador = request.POST.get('identificador_antquir_editar')

		antecedente_quirur = AntecedentesQuirurgico.objects.get(id=identificador)
		antecedente_quirur.descripcion_cirugia = descripcion
		antecedente_quirur.causa = causa
		antecedente_quirur.complicacion = complicacion
		antecedente_quirur.fecha = fecha

		antecedente_quirur.save()

		messages.success(request, '2')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Eliminar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_quirurgico_delete(request,id_antecedente_quirurgico):
	if request.method == 'GET':
		antecedente_quirur = AntecedentesQuirurgico.objects.get(id=id_antecedente_quirurgico)
		antecedente_quirur.delete()

		messages.success(request,'3')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#------------------Medicación Actual ------------------------#

#Crear
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medicacion_actual_create(request,id_paciente):
	if request.method == 'POST':
		paciente = Paciente.objects.get(id=id_paciente)
		antecedente_personal = paciente.antecedentespersonal
		nombre_medicamento = request.POST.get('nombre_medicamento')
		nombre_comercial_me = request.POST.get('nombre_comercial')
		concentracion = request.POST.get('concentracion')
		frecuencia = request.POST.get('frecuencia')
		tipo_administracion = request.POST.get('tipo_adminis')

		new_medicacion_actual = MedicacionActual()
		new_medicacion_actual.nombre_comercial_me = nombre_comercial_me
		new_medicacion_actual.nombre_medicamento = nombre_medicamento
		new_medicacion_actual.concentracion = concentracion
		new_medicacion_actual.frecuencia = frecuencia
		new_medicacion_actual.tipo_administracion = tipo_administracion
		new_medicacion_actual.id_antecedente_personal = antecedente_personal

		new_medicacion_actual.save()

		messages.success(request,'1')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Editar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medicacion_actual_edit(request):
	if request.method == 'POST':
		nombre_medicamento = request.POST.get('nombre_medicamento_editar')
		nombre_comercial_me = request.POST.get('nombre_comercial_editar')
		concentracion = request.POST.get('concentracion_editar')
		frecuencia = request.POST.get('frecuencia_editar')
		tipo_administracion = request.POST.get('tipo_adminis_editar')
		identificador = request.POST.get('identificador_editar_medactual')

		medicacion_actual = MedicacionActual.objects.get(id=identificador)
		medicacion_actual.nombre_comercial_me = nombre_comercial_me
		medicacion_actual.nombre_medicamento = nombre_medicamento
		medicacion_actual.concentracion = concentracion
		medicacion_actual.frecuencia = frecuencia
		medicacion_actual.tipo_administracion = tipo_administracion

		medicacion_actual.save()

		messages.success(request,'2')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Eliminar
@permission_required('consultas.consultas_editar', login_url='')
def antecedente_personal_medicacion_actual_delete(request,id_medicacion_actual):
	if request.method == 'GET':
		medicacion_actual = MedicacionActual.objects.get(id=id_medicacion_actual)
		medicacion_actual.delete()

		messages.success(request,'3')

	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

#Busqueda ajax del codigo (Autocomplete)
def busqueda_codigo(request):
	if request.is_ajax():
		q = request.GET.get('term','').capitalize()
		busqueda = CodigoInternacional.objects.filter(id10__startswith=q).annotate(value=F('dec10'), label=F('id10')).values('id', 'value', 'label')
		resultados = list(busqueda)
		#for r in busqueda:
		#	resultados.append(r.id10)
		data = json.dumps(resultados)
	else:
		data = 'fail'
	mimetype = 'application/json'
	return HttpResponse(data, mimetype)

#Busqueda ajax de la enfermedad (Autocomplete)
def busqueda_enfermedad(request):
	if request.is_ajax():
		q = request.GET.get('term','').capitalize()
		busqueda = CodigoInternacional.objects.filter(dec10__startswith=q).annotate(label=F('dec10'), value=F('id10')).values('id', 'value', 'label')
		resultados = list(busqueda)
		#for r in busqueda:
		#	resultados.append(r.id10)
		data = json.dumps(resultados)
	else:
		data = 'fail'
	mimetype = 'application/json'
	return HttpResponse(data, mimetype)