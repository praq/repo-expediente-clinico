from django.db import models
from apps.consultas.models import *
from django.contrib.auth.models import User
# Create your models here.





class Persona(models.Model):
    #id default
    primer_apellido = models.CharField(max_length=50)
    segundo_apellido = models.CharField(max_length=50)
    primer_nombre = models.CharField(max_length=50)
    segundo_nombre = models.CharField(max_length=50)
    apellido_casada = models.CharField(max_length=50,blank=True,null=True)
    genero = models.CharField(max_length=25)  # podemos ocupar un choise y un tipo de dato charfield
    dui = models.CharField(max_length=11, blank=True,null=True,unique=True)
    telefono = models.CharField(max_length=25, blank=True,null=True,unique=False)
    colonia = models.CharField(max_length=200,blank=True,null=True)
    calle = models.CharField(max_length=200,blank=True,null=True)
    casa = models.CharField(max_length=200,blank=True,null=True)
    nit = models.CharField(max_length=20,blank=True,null=True,unique=True)# pueden ser un varchar por se caracteres por ejemplo 0614-191096-115-7
    isss = models.IntegerField(blank=True,null=True, unique=True) #puede ser un varchar
    fecha_nacimiento = models.DateField(null=True)   #en el modelo fecha_nac
    estado_civil = models.CharField(max_length=30, blank=True)#puede ir en la clase persona
    auth_id = models.OneToOneField(User, on_delete=models.CASCADE)
    departamento_id = models.ForeignKey(Departamento, on_delete=models.CASCADE, default = 0)
    municipio_id = models.ForeignKey(Municipio, on_delete=models.CASCADE, default = 0)


    class Meta:
        db_table = 'Persona'
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        default_permissions = []
        permissions = [
            ('usuarios_control', 'Manejo de los usuarios'),
            ('permisos_control', 'Manejo de los permisos'),
            ('expediente_control', 'Manejo de los Expedientes'),
            ('reportes_control', 'Manejo de Reportería'),
        ]


    def __str__(self):
        return '%s' % (self.nombres)

class Especialidad(models.Model):
    # id default
    nombre_especialidad = models.CharField(max_length=200, unique=True)

    class Meta:
        db_table = 'Especialidad'
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'
        default_permissions = []
        permissions = [
            ('especialidad_control', 'Manejo de las especialidades'),
        ]

    def __str__(self):
        return '%s' %  (self.nombre_especialidad)


class Empleado(models.Model):
    # id default
    tipo_empleado = models.CharField(max_length=50,blank=True) #
    sueldo = models.FloatField()
    cargo = models.CharField(max_length=200)
    fecha_contratacion = models.DateField()
    estado = models.BooleanField() #tambien un choise
    id_persona = models.OneToOneField(Persona, on_delete=models.CASCADE, db_column='id_persona')
    id_especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE,db_column='id_especialidad',blank=True,null=True)
    id_clinica = models.ForeignKey(Clinica, on_delete=models.CASCADE, db_column='id_clinica')

    class Meta:
        db_table = 'Empleado'
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'
        default_permissions = []
        permissions = [
            ('empleados_control', 'Manejo de los empleados'),
        ]

    def __str__(self):
        return '%s' % (self.id)

class Paciente(models.Model):
    # id default
    id_persona = models.OneToOneField(Persona, on_delete=models.CASCADE, db_column='id_persona')
    # falta una edad talves en la clase Persona
    NumExp=models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = 'Paciente'
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'
        default_permissions = []
        permissions = [
            ('expedientes_index', 'Ingresar al listado de expedientes.'),
            ('paciente_expediente','Ingreso del paciente a su expediente.')
        ]

    def __str__(self):
        return '%s' % (self.id)

class Familiar(models.Model):
    # id default
    nombre_familiar = models.CharField(max_length=200)
    antecedente_parentesco = models.CharField(max_length=500, blank=True) #puede o no tener un antecedente por eso blank
    telefono = models.CharField(max_length=500,blank=True,null=True)
    id_paciente= models.ForeignKey(Paciente, on_delete=models.CASCADE, db_column='id_paciente')

    class Meta:
        db_table = 'Familiar'
        verbose_name = 'Familiar'
        verbose_name_plural = 'Familiares'
        default_permissions = []

        def __str__(self):
            return '%s' % (self.nombre_familiar)

class ContactoEmergencia(models.Model):
    # id default
    nombre_contacto= models.CharField(max_length=200)
    telefono = models.CharField(max_length=200, blank=True, null=True)
    parentesco = models.CharField(max_length=200, blank=True, null=True)
    #contraseña = models.CharField(max_length=128) #tendra acceso al sistema o porque una contraseña? si fuera en español por estandar
    id_paciente = models.OneToOneField(Paciente, on_delete=models.CASCADE,db_column='id_paciente') #varios pacientes puede tener el mismo contacto por ejemplo unos hermanos

    class Meta:
        db_table = 'Contacto_emergencia'
        verbose_name = 'Contacto de emergencia'
        verbose_name_plural = 'Contactos de emergencia'
        default_permissions = []

        def __str__(self):
            return '%s' % (self.nombre_contacto)

class CostosServicios(models.Model):
    # id default
    concepto = models.CharField(max_length=250)
    costo = models.FloatField()

    class Meta:
        db_table = 'Costos_servicios'
        verbose_name = 'Costo'
        verbose_name_plural = 'Costos'
        default_permissions = []
        permissions = [
            ('costos_control', 'Manejo de los costos clinicos'),
        ]

        def __str__(self):
            return '%s' % (self.concepto)

#Las abreviaciones en las columnas de la BD son las mismas que usan las enfermeras Ej. fc = Frecuencia Cardiaca
class SignoVital(models.Model):
    #TEMPERATURA: Suponiendo que la temperatura maxima es de 999 grados C, con 2 digitos de precision (Siempre en Celcious)
    temperatura = models.DecimalField(max_digits=5,decimal_places=2,db_column='t')
    #FRECUENCIA CARDIACA: Cantidad de latidos por minuto
    frecuencia_cardiaca =models.IntegerField(db_column='fc')
    #FRECUENCIA RESPIRATORIA: Se expresa en cantidad de respiraciones por minuto
    frecuencia_respiratoria =models.IntegerField(db_column='fr')
    #PRESION ARTERIAL: Se expresa como ##/## milímetros de mercurio ('##'' son enteros de dos digitos)
    #Este se mide a partir de los 3 años, por eso se puede dejar vacio
    presion_arterial =models.CharField(max_length=250,null = True,db_column='pa', blank=True)
    #PESO: En El Salvador ya se mide en Kilogramos (es decir la MASA),
    #peso max de 9999 Kg con 2 decimales de presioción
    peso =models.DecimalField(max_digits=6,decimal_places=2,db_column='peso')
    fecha = models.DateField(auto_now=True)
    #Lo relacione con paciente y no con Consulta porque hay gente que se toma solo los signos vitales por control nada mas. y para facilitar los cruds XD
    paciente = models.ForeignKey(Paciente, on_delete=models.CASCADE,db_column="id_paciente")

    class Meta:
        db_table = 'Signo_vital'
        verbose_name = 'SignoVital'
        verbose_name_plural = 'SignosVitales'
        default_permissions = []
        permissions = [
            ('signos_editar', 'Edición del registro de signos vitales de paciente.'),
        ]

        def __str__(self):
            return '%s' % (self.temperatura)