from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from apps.personas.models import *
from apps.consultas.models import *
from apps.antecedentes.models import *
from django.contrib import messages
from django.utils.dateparse import parse_date
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from datetime import datetime, date

###########################FUNCIONES DE LOS COSTOS#######################################


@permission_required('personas.costos_control', login_url='')
def indexCostos(request):
    costosregistrados = CostosServicios.objects.all()
    contexto = {'costos': costosregistrados}
    return render(request, 'costos/index.html', contexto)


@permission_required('personas.costos_control', login_url='')
def costos_create(request):
    if request.method == 'POST':
        concepto = request.POST.get('concepto')
        costo = request.POST.get('costo')
        new_costo = CostosServicios()
        new_costo.concepto = concepto
        new_costo.costo = costo
        new_costo.save()
        messages.success(request, '1')
        return redirect('indexCostos')
    else:

        return render(request, 'costos/ingresar.html')


@permission_required('personas.costos_control', login_url='')
def costos_edit(request, id_costo):
    cost = CostosServicios.objects.get(id=id_costo)
    if request.method == 'GET':

        contexto = {'costo': cost}
        return render(request, 'costos/editar.html', contexto)

    elif request.method == 'POST':

        conc = request.POST.get('concepto')
        cos = request.POST.get('costo')

        cost.concepto = conc
        cost.costo = cos
        cost.save()
        messages.success(request, '2')
        return redirect('indexCostos')


@permission_required('personas.costos_control', login_url='')
def costos_eliminar(request, id_costo):
    if request.method == 'GET':
        cost = CostosServicios.objects.get(id=id_costo)
        cost.delete()
        messages.success(request, '3')

    return redirect('indexCostos')

###########################FUNCIONES DE EXPEDIENTES#######################################

@permission_required('personas.expedientes_index', login_url='')
def indexExpedientes(request):
    user = request.user

    paciente=None

    if user.groups.filter(name='Doctor').exists():
        expedientesregistrados = Paciente.objects.filter(consulta__id_empleado__id=(
            Empleado.objects.filter(tipo_empleado="Personal Medico",id_persona=(
                Persona.objects.filter(auth_id_id=user.id).first().id
            )).first().id
        ),consulta__fecha_reservacion=date.today())
    elif user.groups.filter(name='Paciente').exists():
        expedientesregistrados = Paciente.objects.filter(id_persona=(
            Persona.objects.get(auth_id_id=user.id).id
        ))
        paciente = True
    elif user.groups.filter(name='Recepcionista').exists() or user.is_superuser:
        expedientesregistrados = Paciente.objects.all()
    else:
        expedientesregistrados = None

    contexto = {'expediente': expedientesregistrados,'paciente': paciente}
    return render(request, 'expediente/index.html', contexto)

@permission_required('personas.expediente_control', login_url='')
def expedientes_create(request):
    if request.method == 'POST':

        nombre1 = request.POST.get('nombre1')
        nombre2 = request.POST.get('nombre2')
        apellido1 = request.POST.get('apellido1')
        apellido2 = request.POST.get('apellido2')
        genero = request.POST.get('genero')
        dui = request.POST.get('dui')
        telefono = request.POST.get('telefono')
        direccion = request.POST.get('direccion')
        nit = request.POST.get('nit')
        isss = request.POST.get('isss')
        fecha_nacimiento = request.POST.get('fecha_nacimiento')
        apellido_casada = request.POST.get('apellido_casada')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')
        call = request.POST.get('calle')
        colo = request.POST.get('colonia')
        casa = request.POST.get('casa')
        usersis = request.POST.get('usersis')
        estci = request.POST.get('estadocivil')

        new_persona = Persona()
        new_persona.primer_nombre = nombre1
        new_persona.segundo_nombre = nombre2
        new_persona.primer_apellido = apellido1
        new_persona.segundo_apellido = apellido1
        new_persona.genero = genero
        new_persona.dui = dui
        new_persona.telefono = telefono
        new_persona.direccion = direccion
        new_persona.nit = nit
        new_persona.isss = isss
        new_persona.fecha_nacimiento = fecha_nacimiento
        new_persona.apellido_casada = apellido_casada
        new_persona.departamento_id_id = dep
        new_persona.municipio_id_id = muni
        new_persona.calle = call
        new_persona.colonia = colo
        new_persona.casa = casa
        new_persona.estado_civil = estci
        new_persona.auth_id_id = usersis
        new_persona.save()
        # Luego de guardado la informacion de la persona guaramos la relacion en la tabla paciente
        new_paciente = Paciente()
        # Esto se puede asi ya  despues de creado el objeto atomaticamente se actualizan los datos y se puede obtener el id de la persona
        new_paciente.id_persona_id = new_persona.id

        # crear numero expediente
        letra = new_persona.primer_apellido[0]
        gene = new_persona.genero[0]
        anio = new_persona.fecha_nacimiento
        year1 = str(anio)
        # propiedad year, month, day no me funciona lo vere despues para mientras...
        a = year1[0]
        b = year1[1]
        c = year1[2]
        d = year1[3]
        codigo = gene + letra
        co = new_persona.auth_id_id
        ram = str(co).zfill(4)
        numm = str(ram)
        num = a+b+c+d+codigo+numm
        print(num)
        new_paciente.NumExp = num
        # no necesito validarlo porque obtengo una primary key y ese sera el correlativo
        new_paciente.save()

        # CREANDO LA INFORMACION De referencia
        nombrecnt = request.POST.get('nombre_contacto')
        antecedentecnt = request.POST.get('parentesco')
        telefonocnt = request.POST.get('telefonoContacto')
        paciente_id = new_paciente.id

        new_cntEmer = ContactoEmergencia()
        new_cntEmer.nombre_contacto = nombrecnt
        new_cntEmer.parentesco = antecedentecnt
        new_cntEmer.telefono = telefonocnt
        new_cntEmer.id_paciente_id = paciente_id

        new_cntEmer.save()

        # CREANDO LA REFERENCIA AL ANTECEDENTE PERSONAL
        new_ante = AntecedentesPersonal()
        new_ante.id_paciente_id = new_paciente.id
        new_ante.save()
        messages.success(request, '1')
        return redirect('indexExpedientes')
    else:
        usersdispo = User.objects.exclude(
            id__in=Persona.objects.order_by().values('auth_id_id').distinct())
        depart = Departamento.objects.all()
        muni = Municipio.objects.all()
        contexto = {'departamentos': depart,
                    'usuarios': usersdispo,
                    'municipios': muni
                    }
        return render(request, 'expediente/ingresar.html', contexto)


@permission_required('personas.expedientes_index', login_url='')
def expedientes_edit(request, id_expediente):
    # obtenemos los usuarios que no han sido asignado a un expediente o trabajador
    usersdispo = User.objects.exclude(
        id__in=Persona.objects.order_by().values('auth_id_id').distinct())
    # obtenemos la informacion principal del expediente por medio de su id
    expEditar = Persona.objects.get(id=id_expediente)
    depart = Departamento.objects.all()
    muni = Municipio.objects.all()
    ############ obteniendo el id del paciente por medio de su id de expediente ############
    paciente = Paciente.objects.get(id_persona=id_expediente)
    # oobtenemos la informacion del familiar de referencia por medio del id del paciente
    faminfo = ContactoEmergencia.objects.get(id_paciente=paciente.id)

    codigo = CodigoInternacional.objects.all()

    if request.method == 'GET':

        contexto = {
            'exp': expEditar,
            'departamentos': depart,
            'usuarios': usersdispo,
            'municipios': muni,
            'famref': faminfo,
            'paciente': paciente,
            'cod': codigo,

        }
        return render(request, 'expediente/editar.html', contexto)

    elif request.method == 'POST':

        nombre1 = request.POST.get('nombre1')
        nombre2 = request.POST.get('nombre2')
        apellido1 = request.POST.get('apellido1')
        apellido2 = request.POST.get('apellido2')
        apellido_casada = request.POST.get('apellido_casada')
        genero = request.POST.get('genero')
        dui = request.POST.get('dui')
        telefono = request.POST.get('telefono')
        direccion = request.POST.get('direccion')
        nit = request.POST.get('nit')
        isss = request.POST.get('isss')
        fecha_nacimiento = request.POST.get('fecha_nacimiento')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')
        call = request.POST.get('calle')
        colo = request.POST.get('colonia')
        casa = request.POST.get('casa')
        usersis = request.POST.get('usersis')
        estci = request.POST.get('estadocivil')
        nombrecnt = request.POST.get('nombre_contacto')

        antecedentecnt = request.POST.get('parentesco')

        telefonocnt = request.POST.get('telefonoContacto')

        expEditar.primer_nombre = nombre1
        expEditar.segundo_nombre = nombre2
        expEditar.primer_apellido = apellido1
        expEditar.segundo_apellido = apellido2
        expEditar.apellido_casada = apellido_casada
        expEditar.genero = genero
        expEditar.dui = dui
        expEditar.telefono = telefono
        expEditar.nit = nit
        expEditar.isss = isss
        expEditar.fecha_nacimiento = fecha_nacimiento
        expEditar.departamento_id_id = dep
        expEditar.municipio_id_id = muni
        expEditar.calle = call
        expEditar.colonia = colo
        expEditar.casa = casa
        expEditar.auth_id_id = usersis
        expEditar.estado_civil = estci

        # Guardamos la informacion general del expediente
        expEditar.save()

        faminfo.nombre_contacto = nombrecnt
        faminfo.parentesco = antecedentecnt
        faminfo.telefono = telefonocnt
        # Guardamos la informacion del familiar de referencia
        faminfo.save()

        messages.success(request, '2')
        return redirect('indexExpedientes')




@permission_required('personas.expediente_control', login_url='')
def expedientes_eliminar(request, id_expediente):
    if request.method == 'GET':
        expediente = Persona.objects.get(id=id_expediente)
        expediente.delete()
        messages.success(request, '3')

    return redirect('indexExpedientes')


@permission_required(['personas.expedientes_index'], login_url='')
def expedientes_detalle(request, id_expediente):
    expDetalle = Persona.objects.get(id=id_expediente)
    usersdispo = User.objects.exclude(
        id__in=Persona.objects.order_by().values('auth_id_id').distinct())
    depart = Departamento.objects.all()
    muni = Municipio.objects.all()
    ############ obteniendo el id del paciente por medio de su id de expediente ############
    paciente = Paciente.objects.get(id_persona=id_expediente)
    # oobtenemos la informacion del familiar de referencia por medio del id del paciente
    faminfo = ContactoEmergencia.objects.get(id_paciente=paciente.id)
    contexto = {'expediente': expDetalle,
                'departamentos': depart,
                'usuarios': usersdispo,
                'municipios': muni,
                'famref': faminfo}
    return render(request, 'expediente/detalles.html', contexto)

####################################### EMPLEADOS ####################################

@permission_required('personas.empleados_control', login_url='')
def indexEmpleados(request):
    # Perso = Persona.objects.filter(
    #     id__in=Empleado.objects.order_by().values('id_persona_id').distinct())
    Perso = Empleado.objects.all()
    contexto = {'personas': Perso}
    return render(request, 'empleados/index.html', contexto)

@permission_required('personas.empleados_control', login_url='')
def empleados_create(request):

    if request.method == 'POST':
        #### AGREGADO LOS DATOS DE PERSONA ###
        nombre1 = request.POST.get('nombre1')
        nombre2 = request.POST.get('nombre2')
        apellido1 = request.POST.get('apellido1')
        apellido2 = request.POST.get('apellido2')
        apeCasada = request.POST.get('apeCasada')
        genero = request.POST.get('genero')
        fechaNaci = request.POST.get('fechaNaci')
        telefono = request.POST.get('telefono')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')
        call = request.POST.get('calle')
        colo = request.POST.get('colonia')
        casa = request.POST.get('casa')
        dui = request.POST.get('dui')
        nit = request.POST.get('nit')
        isss = request.POST.get('isss')
        usersis = request.POST.get('usersis')
        estci = request.POST.get('estadocivil')
        # CREAMOS LA INFORMACION PERSONAL DEL TRABAJADOR
        new_empleado = Persona()
        new_empleado.primer_nombre = nombre1
        new_empleado.segundo_nombre = nombre2
        new_empleado.primer_apellido = apellido1
        new_empleado.segundo_apellido = apellido2
        new_empleado.apellido_casada = apeCasada
        new_empleado.genero = genero
        new_empleado.fecha_nacimiento = fechaNaci
        new_empleado.telefono = telefono
        new_empleado.departamento_id_id = dep
        new_empleado.municipio_id_id = muni
        new_empleado.calle = call
        new_empleado.colonia = colo
        new_empleado.casa = casa
        new_empleado.dui = dui
        new_empleado.nit = nit
        new_empleado.isss = isss
        new_empleado.estado_civil = estci
        new_empleado.auth_id_id = usersis
        new_empleado.save()

        ##### Guardamos la informacion referente al empleado ###
        idper = new_empleado.id
        tipemp = request.POST.get('tipEmpl')
        sldo = request.POST.get('sueldo')
        carg = request.POST.get('cargo')
        fechcontra = request.POST.get('fechacontra')
        estado = request.POST.get('estado')
        idcli = request.POST.get('clinica_id')
        idespe = request.POST.get('especialidad_id')

        newinfo_empleado = Empleado()
        newinfo_empleado.tipo_empleado = tipemp
        newinfo_empleado.sueldo = sldo
        newinfo_empleado.cargo = carg
        newinfo_empleado.fecha_contratacion = fechcontra
        newinfo_empleado.estado = estado
        newinfo_empleado.id_clinica_id = idcli
        newinfo_empleado.id_especialidad_id = idespe
        newinfo_empleado.id_persona_id = idper
        newinfo_empleado.save()
        messages.success(request, '1')
        return redirect('indexEmpleados')
    else:
        usersdispo = User.objects.exclude(
            id__in=Persona.objects.order_by().values('auth_id_id').distinct())
        espec = Especialidad.objects.all()
        clini = Clinica.objects.all()
        depart = Departamento.objects.all()
        muni = Municipio.objects.all()
        contexto = {'usuarios': usersdispo,
                    'especialidades': espec,
                    'clinicas': clini,
                    'departamentos': depart,
                    'municipios': muni}
        return render(request, 'empleados/ingresar.html', contexto)

@permission_required('personas.empleados_control', login_url='')
def empleados_edit(request, id_persona):
    usersdispo = User.objects.exclude(
        id__in=Persona.objects.order_by().values('auth_id_id').distinct())
    infopersona = Persona.objects.get(id=id_persona)
    infoempleado = Empleado.objects.get(id_persona=id_persona)
    espec = Especialidad.objects.all()
    clini = Clinica.objects.all()
    depart = Departamento.objects.all()
    muni = Municipio.objects.all()
    if request.method == 'GET':

        contexto = {'usuarios': usersdispo,
                    'especialidades': espec,
                    'clinicas': clini,
                    'persona': infopersona,
                    'empleado': infoempleado,
                    'departamentos': depart,
                    'municipios': muni
                    }

        return render(request, 'empleados/editar.html', contexto)

    elif request.method == 'POST':
        # obteniendo los datos a actualizar de personas
        nombre1 = request.POST.get('nombre1')
        nombre2 = request.POST.get('nombre2')
        apellido1 = request.POST.get('apellido1')
        apellido2 = request.POST.get('apellido2')
        apeCasada = request.POST.get('apeCasada')
        genero = request.POST.get('genero')
        fechaNaci = request.POST.get('fechaNaci')
        telefono = request.POST.get('telefono')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')
        call = request.POST.get('calle')
        colo = request.POST.get('colonia')
        casa = request.POST.get('casa')
        dui = request.POST.get('dui')
        nit = request.POST.get('nit')
        isss = request.POST.get('isss')
        usersis = request.POST.get('usersis')
        estci = request.POST.get('estadocivil')

        # obteniendo los datos a actualizar del emepleado del formulario
        tipemp = request.POST.get('tipEmpl')
        sldo = request.POST.get('sueldo')
        carg = request.POST.get('cargo')
        fechcontra = request.POST.get('fechacontra')
        estado = request.POST.get('estado')
        idcli = request.POST.get('clinica_id')
        idespe = request.POST.get('especialidad_id')

        # Actualizamos primero el info de la tablapersona
        infopersona.primer_nombre = nombre1
        infopersona.segundo_nombre = nombre2
        infopersona.primer_apellido = apellido1
        infopersona.segundo_apellido = apellido2
        infopersona.apellido_casada = apeCasada
        infopersona.genero = genero
        infopersona.fecha_nacimiento = fechaNaci
        infopersona.telefono = telefono
        infopersona.departamento_id_id = dep
        infopersona.municipio_id_id = muni
        infopersona.calle = call
        infopersona.colonia = colo
        infopersona.casa = casa
        infopersona.dui = dui
        infopersona.nit = nit
        infopersona.isss = isss
        infopersona.estado_civil = estci
        infopersona.save()

        # Actualizamos segundo el info de la empleados

        infoempleado.tipo_empleado = tipemp
        infoempleado.sueldo = sldo
        infoempleado.cargo = carg
        infoempleado.fecha_contratacion = fechcontra
        infoempleado.estado = estado
        infoempleado.id_clinica_id = idcli
        infoempleado.id_especialidad_id = idespe
        infoempleado.save()
        messages.success(request, '2')
        return redirect('indexEmpleados')

@permission_required('personas.empleados_control', login_url='')
def empleados_detalle(request, id_persona):
    usersdispo = User.objects.all()
    infopersona = Persona.objects.get(id=id_persona)
    infoempleado = Empleado.objects.get(id_persona=id_persona)
    espec = Especialidad.objects.all()
    clini = Clinica.objects.all()
    depart = Departamento.objects.all()
    muni = Municipio.objects.all()
    contexto = {'usuarios': usersdispo,
                'especialidades': espec,
                'clinicas': clini,
                'persona': infopersona,
                'empleado': infoempleado,
                'departamentos': depart,
                'municipios': muni
                }
    return render(request, 'empleados/detalle.html', contexto)

@permission_required('personas.empleados_control', login_url='')
def empleado_eliminar(request, id_expediente):
    if request.method == 'GET':
        expediente = Persona.objects.get(id=id_expediente)
        infoempleado = Empleado.objects.get(id_persona_id=expediente.id)
        infoempleado.delete()
        expediente.delete()
        messages.success(request, '3')

    return redirect('indexEmpleados')

###########################FUNCIONES DE ESPECIALIDADES#######################################
@permission_required('personas.especialidad_control', login_url='')
def indexEspecialidades(request):
    especialidadesregistradas = Especialidad.objects.all()
    contexto = {'especialidades': especialidadesregistradas}
    return render(request, 'especialidades/index.html', contexto)

@permission_required('personas.especialidad_control', login_url='')
def especialidad_create(request):
    if request.method == 'POST':

        nombre_especialidad = request.POST.get('nombre_especialidad')
        print(
            f"************************************ESPECIALIDAD: {nombre_especialidad}********************************************")
        new_especialidad = Especialidad()
        new_especialidad.nombre_especialidad = nombre_especialidad
        new_especialidad.save()

        messages.success(request, '1')
        return redirect('indexEspecialidades')
    else:
        return render(request, 'especialidades/ingresar.html')

@permission_required('personas.especialidad_control', login_url='')
def especialidad_edit(request, id_especialidad):
    especialidadEditar = Especialidad.objects.get(id=id_especialidad)
    if request.method == 'GET':

        contexto = {'especialidades': especialidadEditar}
        return render(request, 'especialidades/editar.html', contexto)

    elif request.method == 'POST':

        nombre = request.POST.get('nombre')
        especialidadEditar.nombre_especialidad = nombre
        especialidadEditar.save()

        messages.success(request, '2')
        return redirect('indexEspecialidades')

@permission_required('personas.especialidad_control', login_url='')
def especialidad_delete(request, id_especialidad):
    if request.method == 'GET':
        especialidad = Especialidad.objects.get(id=id_especialidad)
        especialidad.delete()
        messages.success(request, '3')

    return redirect('indexEspecialidades')


#------------------------------ Gestion de SIGNOS VITALES ---------------------------------------#

# Crear
@permission_required('personas.signos_editar', login_url='')
def signos_create(request, id_consulta):
    if request.method == 'POST':
        consulta = Consulta.objects.get(pk=id_consulta)
        t = request.POST.get('temperatura')  # temperatura
        fc = request.POST.get('fc')  # Frecuencia Cardiaca
        fr = request.POST.get('fr')  # Frecuencia Respiratoria
        pa = request.POST.get('pa')  # Presion arterial
        peso = request.POST.get('peso')  # En kg
        print(consulta.id_paciente)
        new_signos = SignoVital.objects.create(
            temperatura=t,
            frecuencia_cardiaca=fc,
            frecuencia_respiratoria=fr,
            presion_arterial=pa,
            peso=peso,
            paciente_id=consulta.id_paciente_id
        )
        messages.success(request, '1')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

# Editar

@permission_required('personas.signos_editar', login_url='')
def signos_edit(request, id_signo):
    if request.method == 'POST':
        signo = SignoVital.objects.get(pk=id_signo)
        print(f"Fecha Registrada {signo.fecha.date()}")
        print(f"Fecha Actuak {date.today()}")
       # VALIDACION
    if (signo.fecha.date() < date.today()):
        messages.success(request, '3')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:

        t = request.POST.get('temperatura_editar')  # temperatura
        fc = request.POST.get('fc_editar')  # Frecuencia Cardiaca
        fr = request.POST.get('fr_editar')  # Frecuencia Respiratoria
        pa = request.POST.get('presion_editar')  # Presion arterial
        peso = request.POST.get('peso_editar')  # En kg
        identificador = request.POST.get('identificador_editar_signos')
        print(f"******IDENTIFICADOR****** {identificador}")
        print(request.POST)

        signo = SignoVital.objects.get(id=identificador)
        signo.temperatura = t
        signo.frecuencia_cardiaca = fc
        signo.frecuencia_respiratoria = fr
        signo.presion_arterial = pa
        signo.peso = peso
        signo.save()

        messages.success(request, '2')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

# Eliminar

@permission_required('personas.signos_editar', login_url='')
def signos_delete(request, id_signo):
    if request.method == 'GET':
        signo = SignoVital.objects.get(pk=id_signo)

        # VALIDACION
        if (signo.fecha.date() < date.today()):
            messages.success(
                request, '4')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            signo.delete()
            messages.success(request, '5')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
