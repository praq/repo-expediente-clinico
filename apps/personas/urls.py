from django.urls import path, include
from apps.personas.views import indexCostos, costos_create, costos_edit, costos_eliminar
from apps.personas.views import indexExpedientes, expedientes_create, expedientes_edit, expedientes_eliminar, expedientes_detalle, empleados_create, empleados_edit, indexEmpleados, empleados_detalle
from apps.personas.views import (
    indexEspecialidades, especialidad_create, especialidad_edit, especialidad_delete,
    signos_create, signos_edit, signos_delete,empleado_eliminar)
from apps.consultas.views import consultas_view, examenes_respondidos_view

urlpatterns = [

    ### URLS QUE MANEJAN LOS COSTOS ###
    path('costos/', indexCostos, name='indexCostos'),
    path('costos/crear/', costos_create, name='costo_crear'),
    path('costos/editar/<int:id_costo>/', costos_edit, name='costo_editar'),
    path('costos/<int:id_costo>/eliminar/',
         costos_eliminar, name='costo_eliminar'),

    ### URLS QUE MANEJAN EL EXPEDIENTE DE PACIENTES ###
    path('expedientes/', indexExpedientes, name='indexExpedientes'),
    path('expedientes/crear/', expedientes_create, name='expediente_crear'),
    path('expedientes/editar/<int:id_expediente>/',
         expedientes_edit, name='expediente_editar'),
    path('expedientes/<int:id_expediente>/eliminar/',
         expedientes_eliminar, name='expediente_eliminar'),
    path('expedientes/detalle/<int:id_expediente>/',
         expedientes_detalle, name='expediente_detalle'),



    ### URLS QUE MANEJAN CONSULTAS DE EXPEDIENTES ###
    path('expedientes/consulta/<int:id_expediente>/',
         consultas_view, name='historial_consulta'),
    ### URLS QUE MANEJAN EXAMENES RESPONDIDOS DE EXPEDIENTES ###
    path('expediente/examenesrespondidos/<int:id_expediente>/',
         examenes_respondidos_view, name='examenes_respondidos_view'),


    ### URLS DE EMPLEADOS ###
    path('empleados/', indexEmpleados, name='indexEmpleados'),
    path('empleados/crear/', empleados_create, name='empleado_crear'),
    path('empleados/detalle/<int:id_persona>/',
         empleados_detalle, name='empleado_detalle'),
    path('empleados/editar/<int:id_persona>/',
         empleados_edit, name='empleado_edit'),
    path('empleados/<int:id_expediente>/eliminar/',
         empleado_eliminar, name='empleado_eliminar'),

    ### URLS QUE MANEJAN LAS ESPECIALIDADES ###
    path('listaespecialidades/', indexEspecialidades, name='indexEspecialidades'),
    path('listaespecialidades/crear/',
         especialidad_create, name='especialidad_crear'),
    path('listaespecialidades/editar/<int:id_especialidad>/',
         especialidad_edit, name='especialidad_editar'),
    path('listaespecialidades/<int:id_especialidad>/eliminar/',
         especialidad_delete, name='especialidad_eliminar'),


    ### URLS QUE MANEJAN LOS ANTECEDENTES ###
    # estan en la APP ANTECEDENTES

    ### URLS QUE MANEJAN LOS SIGNOS VITALES ###
    path('signos/crear/<int:id_consulta>/',
         signos_create, name="signos_crear"),
    path('signos/editar/<int:id_signo>', signos_edit, name="signos_editar"),
    path('signos/<int:id_signo>/eliminar',
         signos_delete, name="signos_eliminar"),
]
