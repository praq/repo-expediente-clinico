from django.contrib import admin
from .models import *
# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ('id','primer_apellido', 'segundo_nombre','primer_nombre','segundo_apellido','genero','dui')
admin.site.register(Persona, PersonaAdmin)

class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ('id','tipo_empleado', 'sueldo', 'fecha_contratacion', 'id_persona', 'id_especialidad')
admin.site.register(Empleado,EmpleadoAdmin)

class PacienteAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_persona')
admin.site.register(Paciente,PacienteAdmin)

class EspecialidadAdmin(admin.ModelAdmin):
    list_display = ('id','nombre_especialidad')
admin.site.register(Especialidad,EspecialidadAdmin)


class ContactoEmergenciaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre_contacto','telefono', 'id_paciente')
admin.site.register(ContactoEmergencia,ContactoEmergenciaAdmin)

class CostosServiciosAdmin(admin.ModelAdmin):
    list_display = ('id','concepto','costo')
admin.site.register(CostosServicios,CostosServiciosAdmin)


