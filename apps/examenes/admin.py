from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(ExamenFisico)
admin.site.register(ExamenHecesQuimico)
admin.site.register(ExamenHecesMicroscopico)
admin.site.register(ExamenHecesMacroscopico)
admin.site.register(ExamenOrinaMicroscopico)
admin.site.register(ExamenOrinaMacroscopico)
admin.site.register(ExamenOrinaQuimico)
admin.site.register(ExamenOrinaCristaluria)
admin.site.register(ExamenHematologico)
admin.site.register(ExamenQuimicoSanguineo)