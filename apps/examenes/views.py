from django.shortcuts import render, redirect
from apps.consultas.models import *
from apps.examenes.models import *
from django.contrib import messages
from datetime import date
from datetime import datetime
from django.contrib.auth.decorators import permission_required

# Create your views here.


@permission_required('consultas.laboratorio_control', login_url='')
def indexExamenes(request):
    ordenesEx = OrdenExamen.objects.all().filter(estado="Pendiente")
    # for dt in ordenesEx:
    #     print(dt.id_consulta.id_paciente.id_persona.primer_nombre)
    contexto = {'ordenes': ordenesEx}
    return render(request, 'laboratorio/index.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def indexExamenesRespondidos(request):
    ordenesEx = OrdenExamen.objects.all().filter(estado="Respondido")

    contexto = {'ordenes': ordenesEx}
    return render(request, 'laboratorio/indexExamenesRespondidos.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def ordenController(request, id_orden):
    ordenesEx = OrdenExamen.objects.get(id=id_orden)

    if ordenesEx.id_tipo_id == 1:
        return redirect('crearFisico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 2:
        return redirect('crearHema', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 3:
        return redirect('crearHemacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 4:
        return redirect('crearHemicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 5:
        return redirect('crearHeQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 6:
        return redirect('crearorinaCri', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 7:
        return redirect('crearorinamacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 8:
        return redirect('crearorinamicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 9:
        return redirect('crearorinaQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 10:
        return redirect('crearQuimicaSa', ordenesEx.id)


@permission_required('consultas.laboratorio_control', login_url='')
def ordenEditarController(request, id_orden):
    ordenesEx = OrdenExamen.objects.get(id=id_orden)

    if ordenesEx.id_tipo_id == 1:
        return redirect('editarFisico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 2:
        return redirect('editarHema', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 3:
        return redirect('editarHemacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 4:
        return redirect('editarHemicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 5:
        return redirect('editarHeQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 6:
        return redirect('editarorinaCri', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 7:
        return redirect('editarorinamacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 8:
        return redirect('editarorinamicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 9:
        return redirect('editarorinaQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 10:
        return redirect('editarQuimicaSa', ordenesEx.id)


def ordenVerController(request, id_orden):
    ordenesEx = OrdenExamen.objects.get(id=id_orden)

    if ordenesEx.id_tipo_id == 1:
        return redirect('verFisico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 2:
        return redirect('verHema', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 3:
        return redirect('verHemacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 4:
        return redirect('verHemicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 5:
        return redirect('verHeQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 6:
        return redirect('verorinaCri', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 7:
        return redirect('verorinamacro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 8:
        return redirect('verorinamicro', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 9:
        return redirect('verorinaQuimico', ordenesEx.id)
    elif ordenesEx.id_tipo_id == 10:
        return redirect('verQuimicaSa', ordenesEx.id)


###########################################################################################################################
#-------------------------------------------------------EXAMEN FISICO-----------------------------------------------------#
@permission_required('consultas.laboratorio_control', login_url='')
def examenfisico_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examenFisico = ExamenFisico()
        new_examenFisico.nombre_archivo = request.POST.get('nombreExamen')
        new_examenFisico.path_store = request.FILES.get('archivo')
        new_examenFisico.id_orden_id = id_orden
        new_examenFisico.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenFisico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenfisico_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    fisico = ExamenFisico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'fisicoE': fisico}
        return render(request, 'laboratorio/ExamenFisico/editar.html', contexto)
    elif request.method == 'POST':
        if request.FILES.get('archivo', False):
            fisico.nombre_archivo = request.POST.get('nombreExamen')
            fisico.path_store = request.FILES.get('archivo')
            fisico.save()
            messages.success(request, '2')
            return redirect('indexLaboratorioRepondidos')
        else:
            fisico.nombre_archivo = request.POST.get('nombreExamen')
            fisico.save()
            messages.success(request, '2')
            return redirect('indexLaboratorioRepondidos')


def examenfisico_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    fisico = ExamenFisico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'fisicoE': fisico}
    return render(request, 'laboratorio/ExamenFisico/ver.html', contexto)


###########################################################################################################################
#-------------------------------------------------------EXAMEN HEMATOLOGICO-----------------------------------------------#
@permission_required('consultas.laboratorio_control', login_url='')
def examenHema_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examenHema = ExamenHematologico()
        new_examenHema.tipo_serie = request.POST.get('tserie')
        new_examenHema.unidad = request.POST.get('unidad')
        new_examenHema.valor_referencia = request.POST.get('vreferencia')
        new_examenHema.id_orden_id = id_orden
        new_examenHema.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenHematologico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenHema_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hema = ExamenHematologico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'hemaE': hema}
        return render(request, 'laboratorio/ExamenHematologico/editar.html', contexto)
    elif request.method == 'POST':

        hema.tipo_serie = request.POST.get('tserie')
        hema.unidad = request.POST.get('unidad')
        hema.valor_referencia = request.POST.get('vreferencia')
        hema.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenHema_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hema = ExamenHematologico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'hemaE': hema}
    return render(request, 'laboratorio/ExamenHematologico/ver.html', contexto)


###########################################################################################################################
#-------------------------------------------------Examen Heces Macroscopico-----------------------------------------------#
@permission_required('consultas.laboratorio_control', login_url='')
def examenHemacro_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examenHemacro = ExamenHecesMacroscopico()
        new_examenHemacro.aspecto = request.POST.get('aspecto')
        new_examenHemacro.consistencia = request.POST.get('consistencia')
        new_examenHemacro.color = request.POST.get('color')
        new_examenHemacro.presencia_de_sangre = request.POST.get('sangre')
        new_examenHemacro.olor = request.POST.get('olor')
        new_examenHemacro.restos_alimenticios = request.POST.get('restos')
        new_examenHemacro.presencia_de_moco = request.POST.get('moco')
        new_examenHemacro.id_orden_id = id_orden
        new_examenHemacro.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenHecesMacroscopico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenHemacro_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesmacro = ExamenHecesMacroscopico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'hecesmacroE': hecesmacro}
        return render(request, 'laboratorio/ExamenHecesMacroscopico/editar.html', contexto)
    elif request.method == 'POST':
        hecesmacro.aspecto = request.POST.get('aspecto')
        hecesmacro.consistencia = request.POST.get('consistencia')
        hecesmacro.color = request.POST.get('color')
        hecesmacro.presencia_de_sangre = request.POST.get('sangre')
        hecesmacro.olor = request.POST.get('olor')
        hecesmacro.restos_alimenticios = request.POST.get('restos')
        hecesmacro.presencia_de_moco = request.POST.get('moco')

        hecesmacro.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenHemacro_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesmacro = ExamenHecesMacroscopico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'hecesmacroE': hecesmacro}
    return render(request, 'laboratorio/ExamenHecesMacroscopico/ver.html', contexto)


###########################################################################################################################
#-------------------------------------------------Examen Heces Microscopico-----------------------------------------------#
@permission_required('consultas.laboratorio_control', login_url='')
def examenHemicro_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examenHemicro = ExamenHecesMicroscopico()
        new_examenHemicro.hematies = request.POST.get('hematies')
        new_examenHemicro.leocucitos = request.POST.get('leucocitos')
        new_examenHemicro.flora_bacteriana = request.POST.get('flora')
        new_examenHemicro.levardura = request.POST.get('levadura')
        new_examenHemicro.id_orden_id = id_orden
        new_examenHemicro.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenHecesMicroscopico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenHemicro_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesmicro = ExamenHecesMicroscopico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'hecesmicroE': hecesmicro}
        return render(request, 'laboratorio/ExamenHecesMicroscopico/editar.html', contexto)
    elif request.method == 'POST':
        hecesmicro.hematies = request.POST.get('hematies')
        hecesmicro.leocucitos = request.POST.get('leucocitos')
        hecesmicro.flora_bacteriana = request.POST.get('flora')
        hecesmicro.levardura = request.POST.get('levadura')

        hecesmicro.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenHemicro_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesmicro = ExamenHecesMicroscopico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'hecesmicroE': hecesmicro}
    return render(request, 'laboratorio/ExamenHecesMicroscopico/ver.html', contexto)

###########################################################################################################################
#-------------------------------------------------Examen Heces Quimico-----------------------------------------------#


@permission_required('consultas.laboratorio_control', login_url='')
def examenHeQuimico_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examenHeQui = ExamenHecesQuimico()
        new_examenHeQui.ph = request.POST.get('ph')
        new_examenHeQui.azucares_reductores = request.POST.get('azucares')
        new_examenHeQui.sangre_oculta = request.POST.get('sangre')
        new_examenHeQui.id_orden_id = id_orden
        new_examenHeQui.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenHecesQuimico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenHeQuimico_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesQui = ExamenHecesQuimico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'hecesQuiE': hecesQui}
        return render(request, 'laboratorio/ExamenHecesQuimico/editar.html', contexto)
    elif request.method == 'POST':
        hecesQui.ph = request.POST.get('ph')
        hecesQui.azucares_reductores = request.POST.get('azucares')
        hecesQui.sangre_oculta = request.POST.get('sangre')
        hecesQui.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenHeQuimico_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    hecesQui = ExamenHecesQuimico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'hecesQuiE': hecesQui}
    return render(request, 'laboratorio/ExamenHecesQuimico/ver.html', contexto)

###########################################################################################################################
#-------------------------------------------------Examen Orina Cristaluria-----------------------------------------------#


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaCri_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examen = ExamenOrinaCristaluria()
        new_examen.uratos_amorfos = request.POST.get('uratos')
        new_examen.acido_urico = request.POST.get('acido')
        new_examen.oxalatos_calcicos = request.POST.get('oxalatos')
        new_examen.fosfatos_calcicos = request.POST.get('fosca')
        new_examen.fosfatos_amorfos = request.POST.get('fosamor')
        new_examen.fosfatos_amonicos = request.POST.get('fosamonico')
        new_examen.riesgos_litogenico = request.POST.get('riesgos')
        new_examen.id_orden_id = id_orden
        new_examen.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenOrinaCristaluria/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaCri_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinaCri = ExamenOrinaCristaluria.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'orinaCriE': orinaCri}
        return render(request, 'laboratorio/ExamenOrinaCristaluria/editar.html', contexto)
    elif request.method == 'POST':
        orinaCri.uratos_amorfos = request.POST.get('uratos')
        orinaCri.acido_urico = request.POST.get('acido')
        orinaCri.oxalatos_calcicos = request.POST.get('oxalatos')
        orinaCri.fosfatos_calcicos = request.POST.get('fosca')
        orinaCri.fosfatos_amorfos = request.POST.get('fosamor')
        orinaCri.fosfatos_amonicos = request.POST.get('fosamonico')
        orinaCri.riesgos_litogenico = request.POST.get('riesgos')
        orinaCri.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenorinaCri_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinaCri = ExamenOrinaCristaluria.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'orinaCriE': orinaCri}
    return render(request, 'laboratorio/ExamenOrinaCristaluria/ver.html', contexto)

###########################################################################################################################
#-------------------------------------------------Examen Orina Macroscopico-----------------------------------------------#


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinamacro_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examen = ExamenOrinaMacroscopico()
        new_examen.color = request.POST.get('color')
        new_examen.aspecto = request.POST.get('aspecto')
        new_examen.sedimento = request.POST.get('sedimento')
        new_examen.id_orden_id = id_orden
        new_examen.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenOrinaMacroscopico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinamacro_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinamacro = ExamenOrinaMacroscopico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'orinamacroE': orinamacro}
        return render(request, 'laboratorio/ExamenOrinaMacroscopico/editar.html', contexto)
    elif request.method == 'POST':
        orinamacro.color = request.POST.get('color')
        orinamacro.aspecto = request.POST.get('aspecto')
        orinamacro.sedimento = request.POST.get('sedimento')
        orinamacro.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenorinamacro_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinamacro = ExamenOrinaMacroscopico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'orinamacroE': orinamacro}
    return render(request, 'laboratorio/ExamenOrinaMacroscopico/ver.html', contexto)

###########################################################################################################################
#-------------------------------------------------Examen Orina Microscopico-----------------------------------------------#


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaMicro_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examen = ExamenOrinaMicroscopico()
        new_examen.uretral = request.POST.get('uretral')
        new_examen.urotelio = request.POST.get('urotelio')
        new_examen.renal = request.POST.get('renal')
        new_examen.leucocitos = request.POST.get('leucocitos')
        new_examen.piocitos = request.POST.get('piocitos')
        new_examen.eritrocitos = request.POST.get('eritrocitos')
        new_examen.bacteria = request.POST.get('bacteria')
        new_examen.parasitos = request.POST.get('parasitos')
        new_examen.funguria = request.POST.get('funguria')
        new_examen.filamento_de_muncia = request.POST.get('filamento')
        new_examen.proteina_uromocoide = request.POST.get('proteina')
        new_examen.cilindros = request.POST.get('cilindros')
        new_examen.id_orden_id = id_orden
        new_examen.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenOrinaMicroscopico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaMicro_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinamicro = ExamenOrinaMicroscopico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'orinamicroE': orinamicro}
        return render(request, 'laboratorio/ExamenOrinaMicroscopico/editar.html', contexto)
    elif request.method == 'POST':
        orinamicro.uretral = request.POST.get('uretral')
        orinamicro.urotelio = request.POST.get('urotelio')
        orinamicro.renal = request.POST.get('renal')
        orinamicro.leucocitos = request.POST.get('leucocitos')
        orinamicro.piocitos = request.POST.get('piocitos')
        orinamicro.eritrocitos = request.POST.get('eritrocitos')
        orinamicro.bacteria = request.POST.get('bacteria')
        orinamicro.parasitos = request.POST.get('parasitos')
        orinamicro.funguria = request.POST.get('funguria')
        orinamicro.filamento_de_muncia = request.POST.get('filamento')
        orinamicro.proteina_uromocoide = request.POST.get('proteina')
        orinamicro.cilindros = request.POST.get('cilindros')
        orinamicro.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenorinaMicro_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinamicro = ExamenOrinaMicroscopico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'orinamicroE': orinamicro}
    return render(request, 'laboratorio/ExamenOrinaMicroscopico/ver.html', contexto)

###########################################################################################################################
#-------------------------------------------------Examen Orina Quimico-----------------------------------------------#


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaQuimico_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examen = ExamenOrinaQuimico()
        new_examen.densidad = request.POST.get('densidad')
        new_examen.ph = request.POST.get('ph')
        new_examen.glucosa = request.POST.get('glucosa')
        new_examen.proteinas = request.POST.get('proteinas')
        new_examen.hemoglobina = request.POST.get('hemoglobina')
        new_examen.cuerpo_cetonico = request.POST.get('cuerpo_cetonico')
        new_examen.pigmento_biliar = request.POST.get('pigmento_biliar')
        new_examen.urobilinogeno = request.POST.get('urobilinogeno')
        new_examen.nitritos = request.POST.get('nitritos')
        new_examen.id_orden_id = id_orden
        new_examen.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenOrinaQuimico/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenorinaQuimico_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinaQuimico = ExamenOrinaQuimico.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'orinaQuimicoE': orinaQuimico}
        return render(request, 'laboratorio/ExamenOrinaQuimico/editar.html', contexto)
    elif request.method == 'POST':
        orinaQuimico.densidad = request.POST.get('densidad')
        orinaQuimico.ph = request.POST.get('ph')
        orinaQuimico.glucosa = request.POST.get('glucosa')
        orinaQuimico.proteinas = request.POST.get('proteinas')
        orinaQuimico.hemoglobina = request.POST.get('hemoglobina')
        orinaQuimico.cuerpo_cetonico = request.POST.get('cuerpo_cetonico')
        orinaQuimico.pigmento_biliar = request.POST.get('pigmento_biliar')
        orinaQuimico.urobilinogeno = request.POST.get('urobilinogeno')
        orinaQuimico.nitritos = request.POST.get('nitritos')
        orinaQuimico.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenorinaQuimico_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    orinaQuimico = ExamenOrinaQuimico.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                'orinaQuimicoE': orinaQuimico}
    return render(request, 'laboratorio/ExamenOrinaQuimico/ver.html', contexto)


###########################################################################################################################
#-------------------------------------------------Examen Orina Sanguineo-----------------------------------------------#
@permission_required('consultas.laboratorio_control', login_url='')
def examenQuimicaSa_create(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    if request.method == 'POST':
        new_examen = ExamenQuimicoSanguineo()
        new_examen.parametro = request.POST.get('parametro')
        new_examen.comentario = request.POST.get('comentario')
        new_examen.unidades = request.POST.get('unidades')
        new_examen.resultado = request.POST.get('resultado')
        new_examen.rango = request.POST.get('rango')
        new_examen.id_orden_id = id_orden
        new_examen.save()
        orden.estado = "Respondido"
        orden.fecha_respuesta = datetime.now()
        orden.save()
        messages.success(request, '1')
        return redirect('indexLaboratorioRepondidos')
    else:
        contexto = {'ordenP': orden}
        return render(request, 'laboratorio/ExamenQuimicoSanguineo/ingresar.html', contexto)


@permission_required('consultas.laboratorio_control', login_url='')
def examenQuimicaSa_editar(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    quimicaSa = ExamenQuimicoSanguineo.objects.get(id_orden_id=id_orden)
    if request.method == 'GET':
        contexto = {'ordenP': orden,
                    'quimicaSaE': quimicaSa}
        return render(request, 'laboratorio/ExamenQuimicoSanguineo/editar.html', contexto)
    elif request.method == 'POST':
        quimicaSa.parametro = request.POST.get('parametro')
        quimicaSa.comentario = request.POST.get('comentario')
        quimicaSa.unidades = request.POST.get('unidades')
        quimicaSa.resultado = request.POST.get('resultado')
        quimicaSa.rango = request.POST.get('rango')
        quimicaSa.save()
        messages.success(request, '2')
        return redirect('indexLaboratorioRepondidos')


def examenQuimicaSa_ver(request, id_orden):
    orden = OrdenExamen.objects.get(id=id_orden)
    quimicaSa = ExamenQuimicoSanguineo.objects.get(id_orden_id=id_orden)
    contexto = {'ordenP': orden,
                  'quimicaSaE':quimicaSa}
    return render(request, 'laboratorio/ExamenQuimicoSanguineo/ver.html', contexto)


#############################################################################################################################
#--------------------------------------BITACORA DE SEGURIDAD DEL SISTEMA ---------------------------------------------------#

def bitacora(request):
    bitacora = Control.objects.all()
    contexto = {'bita': bitacora}
    return render(request, 'bitacora.html', contexto)


##############################################################################################################################
# VIEWS DEL SISTEMA

def stats(request):
    examenes = statExamenes.objects.all()
    tusuarios = totalUsuarios.objects.all()
    templeados = cntEmpleados.objects.all()
    tpaciente = cntPacientes.objects.all()
    tcitas = cntCitas.objects.all()
    useDepart = usrDepart.objects.all()
    espcnt = cntEspecialidad.objects.all()
    cntCli = cntClinica.objects.all()
    salas =  cntSalas.objects.all()
    costosserv = cntcostservicios.objects.all()
    citas = citasDia.objects.all()
    print(citas)
    contexto = {'exa': examenes,
                'usr': tusuarios,
                'emp': templeados,
                'pci': tpaciente,
                'cte': tcitas,
                'udpt': useDepart,
                'cnte': espcnt,
                'ccli': cntCli,
                'csalas': salas,
                'cs': costosserv,
                'ci': citas }
    return render(request, 'stats.html', contexto)
