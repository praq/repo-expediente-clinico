from django.db import models
from django.utils.timezone import now
from apps.consultas.models import *
# Create your models here.

class ExamenFisico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    nombre_archivo = models.CharField(max_length=255,)
    path_store = models.FileField(upload_to="examenfisico")
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_e_fisicoisico'
        verbose_name = 'Examen fisico'
        verbose_name_plural = 'Examenes fisicos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.nombre_archivo)

class ExamenHecesQuimico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    ph= models.FloatField()
    azucares_reductores = models.CharField(max_length=255)
    sangre_oculta = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_heces_quimicos'
        verbose_name = 'Examen de heces quimico'
        verbose_name_plural = 'Examenes de heces quimico'
        default_permissions = []
    def __str__(self):
        return '%s' % (self.id)

class ExamenHecesMicroscopico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    hematies= models.CharField(max_length=255)
    leocucitos = models.CharField(max_length=255)
    flora_bacteriana = models.CharField(max_length=255)
    levardura = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_heces_microscopico'
        verbose_name = 'Examen de heces microscopico'
        verbose_name_plural = 'Examenes de heces microscopico'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenHecesMacroscopico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    aspecto= models.CharField(max_length=255)
    consistencia = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    presencia_de_sangre = models.CharField(max_length=255)
    olor = models.CharField(max_length=255)
    restos_alimenticios = models.CharField(max_length=255)
    presencia_de_moco = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_heces_macroscopico'
        verbose_name = 'Examen de heces macroscopico'
        verbose_name_plural = 'Examenes de heces macroscopico'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenOrinaMicroscopico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    uretral= models.CharField(max_length=255)
    urotelio = models.CharField(max_length=255)
    renal = models.CharField(max_length=255)
    leucocitos = models.CharField(max_length=255)
    piocitos = models.CharField(max_length=255)
    eritrocitos = models.CharField(max_length=255)
    bacteria = models.CharField(max_length=255)
    parasitos = models.CharField(max_length=255)
    funguria = models.CharField(max_length=255)
    filamento_de_muncia = models.CharField(max_length=255)
    proteina_uromocoide = models.CharField(max_length=255)
    cilindros = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_orina_microscopico'
        verbose_name = 'Examen de orina microscopico'
        verbose_name_plural = 'Examenes de orina microscopico'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenOrinaMacroscopico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    color= models.CharField(max_length=255)
    aspecto = models.CharField(max_length=255)
    sedimento = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_orina_macroscopico'
        verbose_name = 'Examen de orina macroscopico'
        verbose_name_plural = 'Examenes de orina macroscopico'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenOrinaQuimico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    densidad = models.FloatField()
    ph = models.FloatField()
    glucosa = models.CharField(max_length=255)
    proteinas = models.CharField(max_length=255)
    hemoglobina = models.CharField(max_length=255)
    cuerpo_cetonico = models.CharField(max_length=255)
    pigmento_biliar = models.CharField(max_length=255)
    urobilinogeno = models.CharField(max_length=255)
    nitritos = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_orina_quimico'
        verbose_name = 'Examen de orina quimico'
        verbose_name_plural = 'Examenes de orina quimico'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenOrinaCristaluria(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    uratos_amorfos = models.CharField(max_length=500)
    acido_urico = models.CharField(max_length=500)
    oxalatos_calcicos = models.CharField(max_length=500)
    fosfatos_calcicos = models.CharField(max_length=500)
    fosfatos_amorfos = models.CharField(max_length=500)
    fosfatos_amonicos = models.CharField(max_length=500)
    riesgos_litogenico = models.CharField(max_length=500)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_orina_cristaluria'
        verbose_name = 'Examen de orina cristaluria'
        verbose_name_plural = 'Examenes de orina cristaluria'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenHematologico(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    tipo_serie= models.CharField(max_length=250)
    unidad = models.CharField(max_length=250)
    valor_referencia = models.CharField(max_length=250)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_e_hematologicotologico'
        verbose_name = 'Examen de hematologia'
        verbose_name_plural = 'Examenes de hematologia'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class ExamenQuimicoSanguineo(models.Model):
    #id default
    id_orden = models.OneToOneField(OrdenExamen, on_delete=models.CASCADE, db_column='id_orden')
    parametro= models.CharField(max_length=250)
    comentario = models.CharField(max_length=255)
    unidades = models.CharField(max_length=255)
    resultado = models.FloatField()
    rango = models.CharField(max_length=255)
    creado_en = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Resultado_examen_quimico_sanguineo'
        verbose_name = 'Examen quimico sanguineo'
        verbose_name_plural = 'Examenes de quimicos sanguineos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class Control(models.Model):
    #id default
    accion = models.CharField(max_length=255)
    Fecha_realizacion = models.DateTimeField(default=now, editable=False)

    class Meta:
        db_table = 'Control'
        verbose_name = 'Control'
        verbose_name_plural = 'Control'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

# CLASE PARA LA VIEW DE LA BASE DE DATOS.
class statExamenes(models.Model):
    id_tipo = models.ForeignKey(TipoExamen, on_delete=models.DO_NOTHING)
    cantidad = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'stats_examenes'

class totalUsuarios(models.Model):
    usuariossistema = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'totuser'

class cntEmpleados(models.Model):
    cntempleado = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'totempleados'

class cntPacientes(models.Model):
    cntpaciente = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'totpacientes'

class cntCitas(models.Model):
    cntconsultas = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'consatendidas'

class usrDepart(models.Model):
    id_departamento_id = models.ForeignKey(Departamento, on_delete=models.DO_NOTHING)
    cantidad = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'usudepart'

class cntEspecialidad(models.Model):
   
    cntespecialidades = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'cntespecialidades'

class cntClinica(models.Model):
       
    cnteclinicas = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'cntclinicas'

class cntSalas(models.Model):
       
    cntsalas = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'conteosalas'

class cntcostservicios(models.Model):
       
    cntcostos = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'contcostos'

class citasDia(models.Model):
    id_turno = models.ForeignKey(Turno, on_delete=models.DO_NOTHING)
    fechaatencion = models.CharField(max_length=250)
    cantidad = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = 'citasatendidadporfecha'