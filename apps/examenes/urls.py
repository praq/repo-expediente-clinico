from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from apps.examenes.views import *
urlpatterns = [
    path('examenes/pedidos/', indexExamenes , name='indexLaboratorio'),
    path('examenes/respondidos/', indexExamenesRespondidos , name='indexLaboratorioRepondidos'),
    path('examenes/pedidos/ingresar/<int:id_orden>/', ordenController , name='controladorOrdenes'),
    path('examenes/pedidos/editar/<int:id_orden>/', ordenEditarController , name='controladorOrdenesEditar'),
    path('examenes/pedidos/ver/<int:id_orden>/', ordenVerController , name='controladorOrdenesVer'),
    ## URLS EXAMEN FISICO
    path('examenes/pedidos/ingresar/<int:id_orden>/fisico', examenfisico_create , name='crearFisico'),
    path('examenes/pedidos/editar/<int:id_orden>/fisico', examenfisico_editar , name='editarFisico'),
    path('examenes/pedidos/ver/<int:id_orden>/fisico', examenfisico_ver , name='verFisico'),
    ## URLS EXAMEN HEMATOLOGICO
    path('examenes/pedidos/ingresar/<int:id_orden>/hematologico', examenHema_create , name='crearHema'),
    path('examenes/pedidos/editar/<int:id_orden>/hematologico', examenHema_editar , name='editarHema'),
    path('examenes/pedidos/ver/<int:id_orden>/hematologico', examenHema_ver , name='verHema'),

    ## URLS EXAMEN HECES MACROSCOPICO
    path('examenes/pedidos/ingresar/<int:id_orden>/hecesMacroscopico', examenHemacro_create , name='crearHemacro'),
    path('examenes/pedidos/editar/<int:id_orden>/hecesMacroscopico', examenHemacro_editar , name='editarHemacro'),
    path('examenes/pedidos/ver/<int:id_orden>/hecesMacroscopico', examenHemacro_ver , name='verHemacro'),
    ## URLS EXAMEN HECES MiCROSCOPICO
    path('examenes/pedidos/ingresar/<int:id_orden>/hecesMicro', examenHemicro_create , name='crearHemicro'),
    path('examenes/pedidos/editar/<int:id_orden>/hecesMicro', examenHemicro_editar , name='editarHemicro'),
    path('examenes/pedidos/ver/<int:id_orden>/hecesMicro', examenHemicro_ver , name='verHemicro'),
    ## URLS EXAMEN HECES QUIMICO
    path('examenes/pedidos/ingresar/<int:id_orden>/hecesQuimico', examenHeQuimico_create , name='crearHeQuimico'),
    path('examenes/pedidos/editar/<int:id_orden>/hecesQuimico', examenHeQuimico_editar , name='editarHeQuimico'),
    path('examenes/pedidos/ver/<int:id_orden>/hecesQuimico', examenHeQuimico_ver , name='verHeQuimico'),
    ## URLS EXAMEN Orina Cristaluria
    path('examenes/pedidos/ingresar/<int:id_orden>/orinaCri', examenorinaCri_create , name='crearorinaCri'),
    path('examenes/pedidos/editar/<int:id_orden>/orinaCri', examenorinaCri_editar , name='editarorinaCri'),
    path('examenes/pedidos/ver/<int:id_orden>/orinaCrio', examenorinaCri_ver , name='verorinaCri'),
     ## URLS EXAMEN Orina macroscopico
    path('examenes/pedidos/ingresar/<int:id_orden>/orinamacro', examenorinamacro_create , name='crearorinamacro'),
    path('examenes/pedidos/editar/<int:id_orden>/orinamacro', examenorinamacro_editar , name='editarorinamacro'),
    path('examenes/pedidos/ver/<int:id_orden>/orinamacro', examenorinamacro_ver , name='verorinamacro'),
     ## URLS EXAMEN Orina microscopico
    path('examenes/pedidos/ingresar/<int:id_orden>/orinaMIcro', examenorinaMicro_create , name='crearorinamicro'),
    path('examenes/pedidos/editar/<int:id_orden>/orinaMIcro', examenorinaMicro_editar , name='editarorinamicro'),
    path('examenes/pedidos/ver/<int:id_orden>/orinaMIcro', examenorinaMicro_ver , name='verorinamicro'),

      ## URLS EXAMEN Orina Quimico
    path('examenes/pedidos/ingresar/<int:id_orden>/orinaQuimico', examenorinaQuimico_create , name='crearorinaQuimico'),
    path('examenes/pedidos/editar/<int:id_orden>/orinaQuimico', examenorinaQuimico_editar , name='editarorinaQuimico'),
    path('examenes/pedidos/ver/<int:id_orden>/orinaQuimico', examenorinaQuimico_ver , name='verorinaQuimico'),

       ## URLS EXAMEN Orina Sanguineo
    path('examenes/pedidos/ingresar/<int:id_orden>/QuimicaSa', examenQuimicaSa_create , name='crearQuimicaSa'),
    path('examenes/pedidos/editar/<int:id_orden>QuimicaSa', examenQuimicaSa_editar , name='editarQuimicaSa'),
    path('examenes/pedidos/ver/<int:id_orden>/QuimicaSa', examenQuimicaSa_ver , name='verQuimicaSa'),
    path('bitacora/general/sistema', bitacora , name='VerBitacora'),
    path('estadisticas/general/sistema', stats , name='Verestadisticas'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)