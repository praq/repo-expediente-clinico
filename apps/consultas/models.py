from django.db import models

# Import Persona, Paciente(importacion circular)
# from apps.antecedentes.models import AntecedentesPersonal
# Import Antecedente Personal (importacion circular)
# Create your models here.
class Departamento(models.Model):
    # id default
    nombreDepartamento = models.CharField(max_length=250)


    class Meta:
        db_table = 'Departamento'
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        default_permissions = []



        def __str__(self):
            return '%s' % (self.concepto)

class Municipio(models.Model):
    # id default
    departamento_id = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    nombreMunicipio = models.CharField(max_length=250)

    class Meta:
        db_table = 'Municipio'
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'
        default_permissions = []



        def __str__(self):
            return '%s' % (self.concepto)
class Turno(models.Model):
    doctores = models.ManyToManyField('personas.Empleado')
    nombre_turno = models.CharField(max_length=255, null=False, default='Turno')
    fecha_inicio = models.DateField(null=False)
    fecha_fin = models.DateField(null=False)
    hora_inicio = models.TimeField(null=False)
    hora_fin = models.TimeField(null=False)
    activo = models.BooleanField(null=False, default=True)
    capacidad = models.IntegerField(null=False)

    class Meta:
        db_table = 'Turno'
        verbose_name = 'Turno'
        verbose_name_plural = 'Turnos'
        default_permissions = []
        permissions = [
            ('turnos_control', 'Manejo de turnos')
        ]

        def __str__(self):
            return '%s' % (self.nombre_turno)


class Consulta(models.Model):
    # default id
    id_paciente = models.ForeignKey('personas.Paciente', on_delete=models.CASCADE, db_column='id_paciente')
    id_empleado = models.ForeignKey('personas.Empleado', on_delete=models.CASCADE, db_column='id_empleado')
    fecha_reservacion = models.DateField()
    paciente_asiste = models.BooleanField(null=False, default=True)
    turno = models.ForeignKey(Turno, on_delete=models.SET_NULL, db_column = 'id_turno', null=True, blank=True)

    #quitar de los modelos los campos redundantes

    # quitar de los modelos los campos redundantes

    class Meta:
        db_table = 'Consulta'
        verbose_name = 'Consulta'
        verbose_name_plural = 'Consultas'
        default_permissions = []
        permissions = [
            ('cita_control', 'Manejo de citas'),
            ('consultas_ver', 'Ingreso al listado de consultas.'),
            ('consultas_editar', 'Edición de los datos de una consulta.'),
        ]

    def __str__(self):
        return '%s' % (self.id)


class CodigoInternacional(models.Model):
    # default id
    id10 = models.CharField(max_length=10)
    dec10 = models.CharField(max_length=400)

    class Meta:
        db_table = 'Codigo_internacional'  # siguiendo un estandar
        verbose_name = 'Codigo internacional'
        verbose_name_plural = 'Codigos internacionales'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class DiagnosticoMedico(models.Model):
    #default id
    #id_codigo = models.ManyToManyField(CodigoInternacional)
    sintomas = models.CharField(max_length=500)
    id_consulta = models.OneToOneField(Consulta,on_delete=models.CASCADE,db_column='id_consulta')
    id_codigo_internacional = models.ManyToManyField(CodigoInternacional) #cambio la relacion


    class Meta:
        db_table = 'Diagnostico_medico'
        verbose_name = 'Diagnostico medico'
        verbose_name_plural = 'Diagnosticos medicos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class Receta(models.Model):
    #default id
    id_diagnostico = models.ForeignKey(DiagnosticoMedico, on_delete=models.CASCADE, db_column='id_diagnostico')
    fecha = models.DateField(auto_now=True)

    class Meta:
        db_table = 'Receta'
        verbose_name = 'Receta'
        verbose_name_plural = 'Recetas'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)

class Medicamento(models.Model):
    # default id
    nombre = models.CharField(max_length=250)
    via_administracion = models.CharField(max_length=250)
    nombre_comercial = models.CharField(max_length=200, blank=True, null=True)
    concentracion = models.CharField(max_length=200, blank=True, null=True)
    frecuencia = models.CharField(max_length=200)
    duracion_tratamiento = models.IntegerField() #en dias
    receta = models.ForeignKey(Receta,on_delete=models.CASCADE, db_column='id_receta') #definir un maximo de medicamentos por receta
    # Revisar la relacion a mano

    class Meta:
        db_table = 'Medicamento'
        verbose_name = 'Medicamento'
        verbose_name_plural = 'Medicamentos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.nombre)

#--> El modelo DetalleReceta queda en desuso porque ya no usaremos catalogo de medicamentos :0 y porque nunca lo actualizamos en el modelo conceptual, al parecer no andabamos tan mal como penso el ing XD

#class DetalleReceta(models.Model):
#    #default id
#    dosis = models.CharField(max_length=100)
#    id_medicamento = models.ForeignKey(Medicamento, on_delete=models.CASCADE, db_column=#'id_medicamento')
#    id_receta = models.ForeignKey(Receta, on_delete=models.CASCADE, db_column='id_receta#')
#
 #   class Meta:
#        db_table = 'Detalle_receta'
#        verbose_name = 'Detalle de receta'
 #       verbose_name_plural = 'Detalles de receta'
#
#    def __str__(self):
#        return '%s' % (self.id)

class Clinica(models.Model):
    # default id
    nombre = models.CharField(max_length=250)
    departamento_id = models.ForeignKey(Departamento, on_delete=models.CASCADE, default = 0)
    municipio_id = models.ForeignKey(Municipio, on_delete=models.CASCADE, default = 0)
    direccion = models.CharField(max_length=200)

    class Meta:
        db_table = 'Clinica'
        verbose_name = 'Clinica'
        verbose_name_plural = 'Clinicas'
        default_permissions = []
        permissions = [
            ('clinica_control', 'Manejo de las clinicas')
        ]

        def __str__(self):
            return '%s' % (self.nombre)


class Sala(models.Model):
    # default id
    id_clinica = models.ForeignKey(Clinica, on_delete=models.CASCADE, db_column='id_clinica')
    nombre_sala = models.CharField(max_length=120)  # nombre
    tipo_sala = models.CharField(max_length=120)  # tipo

    class Meta:
        db_table = 'Sala'
        verbose_name = 'Sala'
        verbose_name_plural = 'Salas'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class Camilla(models.Model):
    # default id
    id_sala = models.ForeignKey(Sala, on_delete=models.CASCADE, db_column='id_sala')
    numero_camilla = models.IntegerField(unique=False, null=False)  # Numero
    disponible = models.BooleanField(default=False)

    class Meta:
        db_table = 'Camilla'
        verbose_name = 'Camilla'
        verbose_name_plural = 'Camillas'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class Ingreso(models.Model):
    # default id
    id_consulta = models.OneToOneField(Consulta, on_delete=models.CASCADE, db_column='id_consulta')
    id_camilla = models.ForeignKey(Camilla, on_delete=models.CASCADE, db_column='id_camilla')
    fecha_ingreso = models.DateField()
    fecha_egreso = models.DateField()

    class Meta:
        db_table = 'Ingreso'
        verbose_name = 'Ingreso'
        verbose_name_plural = 'Ingresos'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.id)


class TipoExamen(models.Model):
    # default id
    nombre = models.CharField(max_length=250)
    disponible = models.BooleanField()

    class Meta:
        db_table = 'Tipo_examen'
        verbose_name = 'Tipo examen'
        verbose_name_plural = 'Tipos de examenes'
        default_permissions = []

    def __str__(self):
        return '%s' % (self.nombre)


class OrdenExamen(models.Model):
    # default id
    id_consulta = models.ForeignKey(Consulta, on_delete=models.CASCADE,db_column='id_consulta')
    id_tipo = models.ForeignKey(TipoExamen, on_delete=models.CASCADE, db_column='id_tipo')
    estado = models.CharField(max_length=100)
    fecha_asignacion = models.DateField(null=False,auto_now_add=True)
    fecha_respuesta = models.DateField(null=True)

    class Meta:
        db_table = 'Orden_examen'
        verbose_name = 'Orden de examen'
        verbose_name_plural = 'Ordenes de examen'
        default_permissions = []
        permissions = [
            ('laboratorio_control', 'Manejo del laboratorio clinico'),
            ('examenes_ver','Ver los resultados de examenes de un paciente.'),
        ]

    def __str__(self):
        return '%s' % (self.id)
