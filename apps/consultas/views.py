from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from apps.consultas.models import *
from apps.personas.models import *
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
import json
import datetime
from django.utils import timezone


###########################FUNCIONES DE clinica#######################################


@permission_required('consultas.clinica_control', login_url='')
def indexClinica(request):
    clinicasregistradas = Clinica.objects.all()
    contexto = {'clinicas': clinicasregistradas}
    return render(request, 'clinicas/index.html', contexto)


@permission_required('consultas.clinica_control', login_url='')
def clinica_create(request):
    if request.method == 'POST':
        nom = request.POST.get('nombre')
        dire = request.POST.get('direccion')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')
        new_clinica = Clinica()
        new_clinica.nombre = nom
        new_clinica.departamento_id_id = dep
        new_clinica.municipio_id_id = muni
        new_clinica.direccion = dire
        new_clinica.save()
        messages.success(request, '1')
        return redirect('indexClinica')

    else:
        depart = Departamento.objects.all()
        muni = Municipio.objects.all()
        contexto = {'departamentos': depart,
                    'municipios': muni
                    }
        return render(request, 'clinicas/ingresar.html', contexto)


@permission_required('consultas.clinica_control', login_url='')
def clinica_edit(request, id_clinica):
    cli = Clinica.objects.get(id=id_clinica)
    depart = Departamento.objects.all()
    muni = Municipio.objects.all()
    if request.method == 'GET':

        contexto = {'clinica': cli,
                    'departamentos': depart,
                    'municipios': muni
                    }
        return render(request, 'clinicas/editar.html', contexto)

    elif request.method == 'POST':

        nom = request.POST.get('nombre')
        dire = request.POST.get('direccion')
        dep = request.POST.get('departamento')
        muni = request.POST.get('municipio')

        cli.nombre = nom
        cli.departamento_id_id = dep
        cli.municipio_id_id = muni
        cli.direccion = dire
        cli.save()
        messages.success(request, '2')
        return redirect('indexClinica')


@permission_required('consultas.clinica_control', login_url='')
def clinica_eliminar(request, id_clinica):
    if request.method == 'GET':
        cli = Clinica.objects.get(id=id_clinica)
        cli.delete()
        messages.success(request, '3')

    return redirect('indexClinica')


# Codigo internacional
def codigo_internacional(request):
    codigo = CodigoInternacional.objects.all()
    contexto = {'cod': codigo}
    return render(request, 'clinicas/indexCodigo.html', contexto)


# ------------------------------------- Gestion de Turnos ---------------------------------------- #
# ------Index
@permission_required('consultas.turnos_control', login_url='')
def indexTurno(request):
    turnosregistrados = Turno.objects.all()
    for turno in turnosregistrados:
        turno.fecha_inicio = turno.fecha_inicio.strftime("%d/%b/%Y")
        turno.fecha_fin = turno.fecha_fin.strftime("%d/%b/%Y")
        turno.hora_inicio = turno.hora_inicio.strftime("%H:%M")
        turno.hora_fin = turno.hora_fin.strftime("%H:%M")
    contexto = {'turnos': turnosregistrados}
    return render(request, 'turnos/index.html', contexto)


# ------Ingresar
@permission_required('consultas.turnos_control', login_url='')
def turno_create(request):
    if request.method == 'GET':
        doctores = Empleado.objects.filter(tipo_empleado="Personal Medico")
        contexto = {'doctores': doctores}
        return render(request, 'turnos/ingresar.html', contexto)

    elif request.method == 'POST':
        doctores = Empleado.objects.filter(
            id__in=request.POST.getlist('doctores'))
        new_turno = Turno()
        new_turno.nombre_turno = request.POST.get('nombre')
        new_turno.hora_inicio = request.POST.get('hora_inicio')
        new_turno.hora_fin = request.POST.get('hora_fin')
        new_turno.fecha_inicio = request.POST.get('fecha_inicio')
        new_turno.fecha_fin = request.POST.get('fecha_fin')
        new_turno.capacidad = request.POST.get('capacidad')
        new_turno.save()
        if len(doctores) > 0:
            new_turno.doctores.add(*doctores)
        messages.success(request, '1')
        return redirect('indexTurno')


# ------Editar
@permission_required('consultas.turnos_control', login_url='')
def turno_edit(request, id_turno):
    turno = Turno.objects.get(pk=id_turno)
    turno.hora_inicio = turno.hora_inicio.strftime("%H:%M")
    turno.hora_fin = turno.hora_fin.strftime("%H:%M")
    asignados = turno.doctores.all()

    if request.method == 'GET':

        # TODO: Verificar si existen citas para el turno a editar
        # num_consultas = Consulta.objects.filter(
        #     fecha_reservacion__range=(turno.fecha_inicio, turno.fecha_fin)).count()
        num_consultas = Consulta.objects.filter(turno_id = id_turno).count()

        if(num_consultas) == 0:
            doctores = Empleado.objects.filter(tipo_empleado="Personal Medico")
            contexto = {
                'turno': turno,
                'doctores': doctores,
                'asignados': asignados
            }
            return render(request, 'turnos/editar.html', contexto)
        else:
            messages.success(request, '5')
            return redirect('indexTurno')

        # num_consultas = Consulta.objects.filter(fecha_reservacion__range=(turno.fecha_inicio, turno.fecha_fin)).count()
        doctores = Empleado.objects.filter(tipo_empleado="Personal Medico")
        contexto = {
            'turno': turno,
            'doctores': doctores,
            'asignados': asignados
        }
        return render(request, 'turnos/editar.html', contexto)

    elif request.method == 'POST':
        ids_doctores = request.POST.getlist('doctores')
        doctores = Empleado.objects.filter(id__in=ids_doctores)

        turno.nombre_turno = request.POST.get('nombre')
        turno.hora_inicio = request.POST.get('hora_inicio')
        turno.hora_fin = request.POST.get('hora_fin')
        turno.fecha_inicio = request.POST.get('fecha_inicio')
        turno.fecha_fin = request.POST.get('fecha_fin')
        turno.capacidad = request.POST.get('capacidad')
        turno.doctores.set(doctores)
        turno.save()
        messages.success(request, '4')
        return redirect('indexTurno')


# ------Eliminar
@permission_required('consultas.turnos_control', login_url='')
def turno_eliminar(request, id_turno):
    # TODO: Verificar si existen citas para el turno a eliminar
    if request.method == 'GET':
        turno = Turno.objects.get(pk=id_turno)
        num_consultas = Consulta.objects.filter(turno=turno).count()
        print('El numero de consultaas para el turno a eliminar es: ',
              str(num_consultas))
        if num_consultas == 0:
            turno.delete()
            messages.success(request, '3')
        else:
            messages.success(request, '4')

    return redirect('indexTurno')

    # num_consultas = Consulta.objects.filter(fecha_reservacion__range=(turno.fecha_inicio, turno.fecha_fin)).count()
    turno.delete()
    messages.success(request, '3')
    return redirect('indexTurno')


#------------------------------------- Gestion de Recetas ---------------------------------------- #
# Crear
@permission_required('consultas.consultas_editar', login_url='')
def receta_create(request, id_diagnostico):
    if request.method == 'POST':
        via_admin = request.POST.getlist('via_admin')
        nombre_comer = request.POST.getlist('nombre_comer')
        concentra = request.POST.getlist('concentra')
        frec = request.POST.getlist('frec')
        nombre = request.POST.getlist('nombre')
        duracion = request.POST.getlist('duracion')
        diagnostico = DiagnosticoMedico.objects.get(pk=id_diagnostico)

        new_receta = Receta()
        new_receta.id_diagnostico = diagnostico
        new_receta.save()

        for i in range(0, len(nombre)):
            new_medicamento = Medicamento()
            new_medicamento.nombre = nombre[i]
            new_medicamento.via_administracion = via_admin[i]
            new_medicamento.nombre_comercial = nombre_comer[i]
            new_medicamento.concentracion = concentra[i]
            new_medicamento.frecuencia = frec[i]
            new_medicamento.duracion_tratamiento = duracion[i]
            new_medicamento.receta = new_receta
            new_medicamento.save()

        messages.success(request, '3')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@permission_required('consultas.consultas_editar', login_url='')
def receta_edit(request):  # OJO TE QUEDASTE EN EDIT DE RECETA ######
    print("HOLA LLEGUE A RECETA EDITAR")
    if request.method == 'POST':
        id_receta = request.POST.get('id_receta')
        receta = Receta.objects.get(pk=id_receta)
        via_admin = request.POST.getlist('via_admin_editar')
        nombre_comer = request.POST.getlist('nombre_comer_editar')
        concentra = request.POST.getlist('concentra_editar')
        frec = request.POST.getlist('frec_editar')
        nombre = request.POST.getlist('nombre_editar')
        duracion = request.POST.getlist('duracion_editar')

        receta.medicamento_set.all().delete()
        print(f"{nombre}")

        for i in range(0, len(nombre)):
            new_medicamento = Medicamento()
            new_medicamento.nombre = nombre[i]
            new_medicamento.via_administracion = via_admin[i]
            new_medicamento.nombre_comercial = nombre_comer[i]
            new_medicamento.concentracion = concentra[i]
            new_medicamento.frecuencia = frec[i]
            new_medicamento.duracion_tratamiento = duracion[i]
            new_medicamento.receta = receta
            new_medicamento.save()

        messages.success(request, '3')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@permission_required('consultas.consultas_editar', login_url='')
def receta_eliminar(request, id_receta):
    if request.method == 'GET':
        receta = Receta.objects.get(pk=id_receta)
        receta.delete()
        messages.success(request, '3')

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def receta_ajax(request, id_receta):
    if request.is_ajax():

        resultados = Medicamento.objects.filter(receta_id__id=id_receta)
        print(f"RESULTADO AJAX {resultados}")
        resultados = list(resultados.values())
        data = json.dumps(resultados)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


#------------------------------------- Registro de consultas ---------------------------------------- #

# -----Muestras las consultas correspondientes de cada expediente
@ permission_required('consultas.consultas_ver', login_url='')
def consultas_view(request, id_expediente):
    consulta = Consulta.objects.filter(id_paciente__id=id_expediente)
    paciente = Paciente.objects.filter(pk=id_expediente)
    time = datetime.datetime.now()

    contexto = {'historialconsulta': consulta,
                'pacientes': paciente,
                'time': time
                }
    return render(request, 'consultas/index.html', contexto)


# ------edita la consulta ya creada en el historial de consultas
@ permission_required('consultas.consultas_ver', login_url='')
def consultas_edit(request, id_consulta, todas):
    consulta = Consulta.objects.get(pk=id_consulta)
    diagnostico = DiagnosticoMedico.objects.filter(id_consulta=id_consulta).first()
    internacional = CodigoInternacional.objects.all()[:10]
    sala = Sala.objects.all()
    camilla = Camilla.objects.all()
    # recupero el ingreso unico de la consulta
    ingreso = Ingreso.objects.filter(id_consulta=id_consulta)
    tipo = TipoExamen.objects.all()
    ordenexamen = OrdenExamen.objects.filter(
        id_consulta=id_consulta, estado='Pendiente')
    if todas == 1:
        recetas = Receta.objects.filter(
            id_diagnostico__id__in=(
                DiagnosticoMedico.objects.filter(id_consulta__id__in=(
                    Consulta.objects.filter(id_paciente=consulta.id_paciente).values('id'))
                ).values('id')
            )
        )
    else:
        recetas = Receta.objects.filter(
            id_diagnostico__id__in=(
                DiagnosticoMedico.objects.filter(id_consulta__id=id_consulta).values('id'))
        )

    if request.method == 'GET':
        contexto = {
            'todas': todas,
            'recetas': recetas,
            'consulta': consulta,
            'internacional': internacional,
            'sala': sala,
            'camilla': camilla,
            'ingreso': ingreso,
            'tipo': tipo,
            'ordenexamen': ordenexamen,
            'diagnostico': diagnostico,
        }
        return render(request, 'consultas/edit_consulta.html', contexto)

    elif request.method == 'POST' or has_permission('consultas.consultas_editar'):
        fecha = request.POST.get('fecha')
        consulta.fecha_reservacion = fecha
        consulta.id_empleado_id = consulta.id_empleado_id
        consulta.id_paciente_id = consulta.id_paciente_id
        consulta.save()

        # le mande id agregando _id
        return redirect('historial_consulta', consulta.id_paciente.id_persona_id)

# -- crear diagnonstico #no agrega el id_codigo_internacional # ni puedo traer los datos a la tabla


@ permission_required('consultas.consultas_editar', login_url='')
# cada consulta solo tiene UN diagnóstico
def diagnostico_create(request, id_consulta):
    if request.method == 'POST':
        consulta = Consulta.objects.get(id=id_consulta)
        sintomas = request.POST.get('sintomas')
        ids_codigo = request.POST.getlist('ids_codigos')

        codigo = CodigoInternacional.objects.filter(id__in=ids_codigo)
        print(codigo)
        new_diagnostico = DiagnosticoMedico()
        new_diagnostico.sintomas = sintomas
        new_diagnostico.id_consulta_id = consulta.id
        new_diagnostico.save()
        new_diagnostico.id_codigo_internacional.add(*codigo)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

# editar Diagnóstico


# cada consulta solo tiene UN diagnóstico
def diagnostico_edit(request, id_diagnostico):
    if request.method == 'POST':
        diagnostico = DiagnosticoMedico.objects.get(pk=id_diagnostico)
        sintomas = request.POST.get('sintomas_editar')
        ids_codigo = request.POST.getlist('ids_codigos_editar')

        codigo = CodigoInternacional.objects.filter(id__in=ids_codigo)
        diagnostico.sintomas = sintomas
        diagnostico.save()
        diagnostico.id_codigo_internacional.clear()
        # add para agregar, set para reemplazar
        diagnostico.id_codigo_internacional.add(*codigo)

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@ permission_required('consultas.consultas_editar', login_url='')
def ingreso_create(request, id_consulta):
    if request.method == 'POST':
        consulta = Consulta.objects.get(id=id_consulta)
        fe_ingreso = request.POST.get('fecha_ingreso_formato')
        fe_egreso = request.POST.get('fecha_egreso_formato')
        camilla_id = request.POST.get('camilla_id')
        camilla = Camilla.objects.get(id=camilla_id)

        new_ingreso = Ingreso()
        new_ingreso.fecha_ingreso = fe_ingreso
        new_ingreso.fecha_egreso = fe_egreso
        new_ingreso.id_consulta_id = consulta.id
        new_ingreso.id_camilla_id = camilla.id
        new_ingreso.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# ---Editar ingreso
@ permission_required('consultas.consultas_editar', login_url='')
def ingreso_editar(request):
    if request.method == 'POST':
        fe_ingreso = request.POST.get('fe_ingreso_editar')
        fe_egreso = request.POST.get('fe_egreso_editar')
        camilla = request.POST.get('camilla_editar')
        sala = request.POST.get('sala_editar')
        identificador = request.POST.get('identificador_editar')

        ingreso = Ingreso.objects.get(id=identificador)
        ingreso.fecha_ingreso = fe_ingreso
        ingreso.fecha_egreso = fe_egreso
        ingreso.id_camilla_id = camilla

        ingreso.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# ---elimino ingreso me falta update sacarlo de un modal
@ permission_required('consultas.consultas_editar', login_url='')
def ingreso_eliminar(request, id_ingreso):
    if request.method == 'GET':
        ingreso = Ingreso.objects.get(id=id_ingreso)
        ingreso.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# --- crear orden de examen
@ permission_required('consultas.consultas_editar', login_url='')
def ordenExamen_create(request, id_consulta):
    if request.method == 'POST':
        consulta = Consulta.objects.get(id=id_consulta)
        tipo_id = request.POST.get('tipo_id')
        tipo = TipoExamen.objects.get(id=tipo_id)

        new_order = OrdenExamen()
        new_order.estado = 'Pendiente'
        new_order.id_consulta_id = consulta.id
        new_order.id_tipo_id = tipo.id

        new_order.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def orden_eliminar(request, id_orden):
    if request.method == 'GET':
        orden = OrdenExamen.objects.get(id=id_orden)
        orden.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# ----INDEX EXAMENES RESPONDIDOS

# @permission_required('consultas.examenes_ver ', login_url='')
def examenes_respondidos_view(request, id_expediente):
    examenes = OrdenExamen.objects.filter(estado='Respondido',
                                          id_consulta__id__in=(
                                              Consulta.objects.filter(
                                                  id_paciente__id_persona=id_expediente).values('id')
                                          ).values('id')
                                          )
    consulta = Consulta.objects.filter(id_paciente__id_persona=id_expediente)

    print(consulta)
    print(examenes)
    contexto = {'examenes': examenes}
    return render(request, 'consultas/examenes_index.html', contexto)


# ------Index Consultas programadas
@ permission_required(['consultas.cita_control', 'programar_consulta'], login_url='')
def indexConsultaProgramada(request):
    if request.method == 'GET':
        paciente = None
        try:
            user = request.user
            persona = Persona.objects.get(auth_id_id=user.id)
            paciente = Paciente.objects.get(id_persona=persona.id)
        except:
            print('Se listaran todas las consultas programadas')
        if paciente:
            consultas = Consulta.objects.filter(fecha_reservacion__gte=timezone.now(),
                                                id_paciente=paciente.id).order_by('fecha_reservacion')
        else:
            consultas = Consulta.objects.filter(
                fecha_reservacion__gte=timezone.now()).order_by('fecha_reservacion')
        for consulta in consultas:
            consulta.fecha_reservacion = consulta.fecha_reservacion.strftime(
                "%d/%b/%Y")
        contexto = {
            'consultas': consultas,
        }
        return render(request, 'consultas/indexProgramadas.html', contexto)


# ------Programar consulta
@permission_required(['consultas.cita_control', 'programar_consulta'], login_url='')
def consulta_create(request):
    if request.method == 'GET':
        pacientes = Paciente.objects.all()
        turnos = Turno.objects.filter(activo=True)
        for turno in turnos:
            turno.fecha_inicio = turno.fecha_inicio.strftime("%d/%b/%Y")
            turno.fecha_fin = turno.fecha_fin.strftime("%d/%b/%Y")
            turno.hora_inicio = turno.hora_inicio.strftime("%H:%M")
            turno.hora_fin = turno.hora_fin.strftime("%H:%M")
        doctores = Turno.doctores.through.objects.all()
        contexto = {
            'turnos': turnos,
            'pacientes': pacientes,
            'doctores': doctores,
        }
        return render(request, 'consultas/programar.html', contexto)

    elif request.method == 'POST':
        paciente = Paciente.objects.get(id=request.POST.get('paciente'))
        doctor = Empleado.objects.get(id=request.POST.get('doctor'))
        turno = Turno.objects.get(id=request.POST.get('turno'))
        fecha = request.POST.get('fecha')
        numConsultas = Consulta.objects.filter(
            fecha_reservacion=fecha, id_empleado=doctor, turno=turno).count()
        otraCita = Consulta.objects.filter(
            id_paciente=paciente, fecha_reservacion=fecha, turno=turno).count() > 0
        lleno = numConsultas < turno.capacidad
        if otraCita:
            messages.success(request, '3')
            return redirect('indexConsulta_Programada')
        elif lleno:
            new_consulta = Consulta()
            new_consulta.id_paciente = paciente
            new_consulta.id_empleado = doctor
            new_consulta.fecha_reservacion = fecha
            new_consulta.turno = turno
            new_consulta.save()
            messages.success(request, '1')
            return redirect('indexConsulta_Programada')
        else:
            messages.success(request, '2')
            return redirect('indexConsulta_Programada')


# ------ Eliminar consulta programada
@permission_required(['consultas.cita_control', 'programar_consulta'], login_url='')
def consulta_eliminar(request, id_consulta):
    if request.method == 'GET':
        consulta = Consulta.objects.get(pk=id_consulta)
        consulta.delete()
        messages.success(request, '4')
        return redirect('indexConsulta_Programada')
