from django.contrib import admin
from .models import *
# Register your models here.

class ConsultaAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_paciente','id_empleado','fecha_reservacion')
admin.site.register(Consulta, ConsultaAdmin)

class CodigoInternacionalAdmin(admin.ModelAdmin):
    list_display = ('id', 'id10','dec10')
admin.site.register(CodigoInternacional, CodigoInternacionalAdmin)

class DiagnosticoMedicoAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_consulta','sintomas')
admin.site.register(DiagnosticoMedico, DiagnosticoMedicoAdmin)

class RecetaAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_diagnostico')
admin.site.register(Receta, RecetaAdmin)

class MedicamentoAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','via_administracion')
admin.site.register(Medicamento, MedicamentoAdmin)

class ClinicaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','direccion')
admin.site.register(Clinica, ClinicaAdmin)

class SalaAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_clinica','nombre_sala','tipo_sala')
admin.site.register(Sala, SalaAdmin)

class CamillaAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_sala','numero_camilla')
admin.site.register(Camilla, CamillaAdmin)

class IngresoAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_consulta','id_camilla','fecha_ingreso', 'fecha_egreso')
admin.site.register(Ingreso, IngresoAdmin)

class OrdenExamenAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_consulta','id_tipo','estado')
admin.site.register(OrdenExamen, OrdenExamenAdmin)

class TipoExamenAdmin(admin.ModelAdmin):
    list_display = ('id', 'nombre','disponible')
admin.site.register(TipoExamen, TipoExamenAdmin)

admin.site.register(Turno)

