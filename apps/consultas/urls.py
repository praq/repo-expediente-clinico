from django.urls import path, include
from apps.consultas.views import *

urlpatterns = [
    # URLS QUE MANEJAN LAS CLINICAS
    path('clinica/', indexClinica, name='indexClinica'),
    path('clinica/crear/', clinica_create, name='clinica_crear'),
    path('clinica/editar/<int:id_clinica>/',
         clinica_edit, name='clinica_editar'),
    path('clinica/<int:id_clinica>/eliminar/',
         clinica_eliminar, name='clinica_eliminar'),

    # URLS DE CODIGO INTERNACIONAL
    path('codigoInternacional/', codigo_internacional, name='codInter'),

    #-------------------------- Gestion de Recetas -------------------------#
    path('receta/crear/<int:id_diagnostico>',
         receta_create, name='receta_crear'),
    path('receta/editar/', receta_edit, name='receta_editar'),
    path('receta/<int:id_receta>/eliminar/',
         receta_eliminar, name='receta_eliminar'),
    path('receta/ajax/<int:id_receta>',
         receta_ajax, name='receta_ajax'),

    # URLS DE GESTION DE TURNOS
    path('turno/', indexTurno, name='indexTurno'),
    path('turno/crear/', turno_create, name='turno_crear'),
    path('turno/editar/<int:id_turno>/', turno_edit, name='turno_editar'),
    path('turno/<int:id_turno>/eliminar/',
         turno_eliminar, name='turno_eliminar'),

    # -------------------------- Gestion de Recetas -------------------------#
    path('receta/crear/<int:id_diagnostico>',
         receta_create, name='receta_crear'),
    path('receta/editar/<int:id_receta>/', receta_edit, name='receta_editar'),
    path('receta/<int:id_receta>/eliminar/',
         receta_eliminar, name='receta_eliminar'),

    # -------------------------- Consulta -------------------------#
    path('consulta/editar/<int:id_consulta>/<int:todas>',
         consultas_edit, name='consultas_edit'),
    path('diagnostico/crear/<int:id_consulta>',
         diagnostico_create, name='diagnostico_create'),
    path('diagnostico/editar/<int:id_diagnostico>',
         diagnostico_edit, name='diagnostico_editar'),
    path('ingreso/crear/<int:id_consulta>',
         ingreso_create, name='ingreso_create'),
    path('ingreso/editar/', ingreso_editar, name='ingreso_editar'),
    path('ingreso/eliminar/<int:id_ingreso>/eliminar/',
         ingreso_eliminar, name='ingreso_eliminar'),
    path('ordenExamen/crear/<int:id_consulta>',
         ordenExamen_create, name='ordenExamen_create'),
    path('ordenExamen/<int:id_orden>/eliminar',
         orden_eliminar, name='orden_eliminar'),
    path('programadas', indexConsultaProgramada,
         name='indexConsulta_Programada'),
    path('programar', consulta_create, name='consulta_programar'),
    path('<int:id_consulta>/eliminar',
         consulta_eliminar, name='consulta_eliminar'),

]
