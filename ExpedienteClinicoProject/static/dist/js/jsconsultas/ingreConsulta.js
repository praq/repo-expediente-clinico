$(document).ready(function() {

    const $depa = $('#turno');
    const $muni = $('#doctor');
    
    $depa.change(function() {
        $muni.val('');
    
        $muni.prop('disabled', !Boolean($depa.val()));
        $muni.find('option[data-region]').hide();
        $muni.find('option[data-region="' + $depa.val() + '"]').show();
    });

});