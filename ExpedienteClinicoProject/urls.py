"""ExpedienteClinicoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.shortcuts import render
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from django.conf import settings

@login_required
def index(request):
    return render(request, 'pages/index.html')

urlpatterns = [
    path('',auth_views.LoginView.as_view(template_name='account/prueba.html'), name='login'),
    path('home/', index, name='home'),
    path('admin/', admin.site.urls),
    path('accounts/', include('apps.account.urls')),
    path('clinica/', include('apps.personas.urls'), name='clinica'),
    path('consultas/', include('apps.consultas.urls'), name='consultas'),
    # path('pacientes/', include('apps.personas.urls'), name='pacientes'),
    # path('especialidades/', include('apps.personas.urls'), name='especialidades')
    path('antecedentes/', include('apps.antecedentes.urls'), name='antecedentes'),#☠
    path('laboratorio/', include('apps.examenes.urls'), name='laboratorio')
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)