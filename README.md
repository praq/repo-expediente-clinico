# Repo Expediente Clinico

**Repositorio creado para realizar el proyecto de la materia Bases de Datos, del ciclo I -2020**

*Trabajo realizado en medio de una Pandemia :)*

[Enlace para ver los pasos de instalacion de Django](https://matr1x.cubava.cu/como-instalar-django/)

Se ha utilizado
- Python 3.6
- Django 2.2

### *Pasos a seguir*

#### Entorno Virtual

- Instalar el entorno virtual

`pip install virtualenvsudo pip3 install virtualenv`

- Crear el entorno virtual

`virtualenv nombre_de_tu_entorno -p python3`

- Para instalar Django

`python -m pip install Django2.2
`

- Activar y desactivar el entorno virtual

`source .nombre_entorno_virtual/bin/activate`

*para desactivarlo*

`deactivate`

### Instalar Django dentro del entorno virtual

- Instalar django

`pip3 install django==2.2`

*para ver que elementos tengo instalados en pip ejecutamos*

`pip freeze`

- Ver opciones de django

`django-admin help`

- Crear proyecto en django

`django-admin startproject nombre_proyecto .`

*Al crear el proyecto se crear el archivo manage.py el cual nunca se debe de tocar*

`django manage.py help`

- Crear el archivo .gitignore que permite añadir archivos que no queremos que se suban al repositorio al hacer push con git

*dentro del proyecto crear el archivo y dentro agregarle lo que sale en este link https://djangowaves.com/tips-tricks/gitignore-for-a-django-project/*

### Servidor esta listo para correr

- Correr servidor

`python manage.py run server`

### Crear la app

`python manage.py startapp nombre_app`

- Configurar archivo settings.py

*En la parte de INSTALLED_APPS, agregar lo siguiente, tomando en cuenta que 'AppConfig', es la clase que se encuentra en el archivo apps.py, y tendra el nombre correspondiente*

`'nombre_app.apps.AppConfig',`

- Dentro de la carpeta nombre_app crear el archivo url.py y dentro importar lo siguiente

```python
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index')
]
```

*Luego* se debe crear el metodo index dentro del archivo views.py

```python
from django.http import HttpResponse

def index(request):
    return HttpResponse('<h1>Hola wapa</h1>')
```

- Dentro de la carpeta nombre_proyecto, acceder al archivo urls.py y agregar el path dentro de urlpatterns

```python
from django.urls import path, include

path('', include('nombre_app.urls')),
```

### Templates

- Configurando dentro del archivo setting.py en la parte de TEMPLATES, agregar lo siguiente

`'DIRS': [os.path.join(BASE_DIR, 'templates')],`

*posterior a eso, crear un folder llamado "templates", el cual estara creado fuera del proyecto y fuera de la app*

*Dentro del folder template, se pueden ordenar los archivos que se estaran utilizando para los templates*

##### Uso de la sintaxis Jinja en los templates

*Para no volver a reescribir codigo en un archivo html, se utiliza jinja*

- Se crea dentro del folder template, un archivo base.html, el cual se podrá hacer un extend de este en otros archivos

- La sintaxis dentro de la base.html, en la parte de body, podria ser 
`{% block content %} {% endblock %}`

- Desde otro archivo se extiende a todo lo que contiene en el base.html

```
{% extends base.html %}

{% block content %}
<h1>Hola</h1>
{% endblock %}
```

### Archivos static

- Dentro del folder nombre_proyecto crear el folder "static" en el cual iran los principales archivos estaticos

*Podran ir los archivos css, js, img y webfonts*

- Ir al archivo de configuracion settings.py y al final del documento, arriba de STATIC_URL, agregar lo siguiente

```
STATIC_ROOT= os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS= [
    os.path.join(BASE_DIR, 'nombre_proyecto/static')
]
```

*Posteriormente correr en la terminal*

`python manage.py collectstatic`

*Se creará una nueva carpeta en el directorio base, llamada static, que contrendra los mismos datos de la carpeta static creada dentro de nombre_proyecto. Dentro de la carpeta static creada anteriormente, aparecera una nueva carpeta llamada admin*

### Referenciar static

*Para usar el especial static helper que creamos anteriormente*
- Luego de haber puesto todos los archivos en la carpeta static, para poder referenciar desde el base.html se hace lo siguiente

```
{% load static %} <!-- Esto va al inicio del html -->
<!-- Luego se agrega lo siguiente a todas las direcciones que estan en la carpeta static -->
<link rel="stylesheet" href="{% static 'css/all.css' %}">
```